async function getIdentityIndexJS(address) {
    return await api.query.identity.identityIndexOf(address)
}

async function getIdentityDataJS(idtyIndex) {
    return await api.query.identity.identities(idtyIndex)
}

async function getIdentityDataFromAddressJS(address) {
    const idtyIndex = await getIdentityIndexJS(address);
    return await api.query.identity.identities(idtyIndex)
}

async function getCurrentUdIndexJS() {
    return await api.query.universalDividend.currentUdIndex()
}

async function getPastReevalsJS() {
    const result = (await api.query.universalDividend.pastReevals()).toString()
    const obj = JSON.parse(result)
    return obj
}

async function connectNodeJS(endpoint) {
    console.log("Connection to endpoint " + endpoint);
    const connected = await settings.connectAll(endpoint);
    return connected
}

async function getBalanceJS(address) {
    const balanceR = await api.query.system.account(address);
    return balanceR['data'];
}

async function getIdentitiesNumberJS() {
    return (await api.query.identity.counterForIdentities()).toString();
}

async function getCertsJS(idtyIndex) {
    const _certsReceiver = (await api.query.certification.storageIdtyCertMeta(idtyIndex.toString())).toString()
    const obj = JSON.parse(_certsReceiver);

    return [+obj['receivedCount'], +obj['issuedCount']];
}

let subBlocProvider = null;

async function subscribeNewBlocsJS(callback) {
    subBlocProvider = await api.rpc.chain.subscribeNewHeads((header) => callback(header));
    return subBlocProvider;
}

async function unSubscribeNewBlocsJS() {
    if (subBlocProvider && typeof subBlocProvider.unsubscribe === 'function') {
        await subBlocProvider.unsubscribe();
        subBlocProvider = null;
    }
}

// Get wallets presents in polkadot.js extension app
async function getInjectedAccountsJS(callback) {
    const allInjected = await walletExtension.web3Enable('Gecko web app');
    console.log(allInjected.length);
    if (allInjected.length === 0) {
        console.log("isWeb3Injected is false ! : " + walletExtension.isWeb3Injected);
        return ["no_extension"];
    }
    const allAccounts = await walletExtension.web3Accounts();

    // walletExtension.web3AccountsSubscribe((injectedAccounts) => {
    //     injectedAccounts.map((account) => {
    //         callback(account);
    //     })
    // });

    return allAccounts;
}

// Pay with a wallet in polkadot.js extension app
async function payExtensionJS(callback, from, to, amount) {
    // finds an injector for an address
    const injector = await walletExtension.web3FromAddress(from);

    api.tx.balances
        .transferKeepAlive(to, amount)
        .signAndSend(from, { signer: injector.signer }, (status) => {
            // console.log(status.status.toString());
            callback(status.toHuman(), from, to, amount);
        }).catch((error) => {
            console.log(':( transaction failed: ' + error);
            callback({ "error": error.toString() }, from, to, amount)
        });
}

// Check if the given address is valid
function isValidAddressJS(address) {
    const isValidAddressPolkadotAddress = () => {
        try {
            encodeAddress(
                isHex(address)
                    ? hexToU8a(address)
                    : decodeAddress(address)
            );

            return true;
        } catch (error) {
            return false;
        }
    };
    return isValidAddressPolkadotAddress();
}

////////////////////////
//////// Account ///////
////////////////////////

// import account (this is not used, prefere use polkadot.js web extension instead)
async function importAccountJS() {
    const { pair, json } = keyring.addUri(mnemonic, 'myStr0ngP@ssworD', { name: 'mnemonic acc' });
}

////////////////////////
//////// Smiths ////////
////////////////////////

//counter of membership
async function getSmithMembershipCounterJS() {
    return (await api.query.smithMembership.counterForMembership()).toString();
}

//counter of authorities
async function getAuthoritiesCounterJS() {
    return (await api.query.authorityMembers.authoritiesCounter()).toString();
}

//indexes of online Authorities (will be parsed List int)
async function getOnlineSmithsJS() {
    return await api.query.authorityMembers.onlineAuthorities();
}

//indexes of incoming Authorities (will be parsed List int)
async function getIncomingAuthoritiesJS() {
    return await api.query.authorityMembers.incomingAuthorities();
}

//indexes of outgoing Authorities (will be parsed List int)
async function getOutgoingAuthoritiesJS() {
    return await api.query.authorityMembers.outgoingAuthorities();
}

//Membership Expiration (will be parsed int)
async function getMembershipExpirationJS(idtyIdenx) {
    var membershipExpiration = await api.query.membership.membership(idtyIdenx.toString());
    membershipExpiration = membershipExpiration.toString();
    try {
        obj = JSON.parse(membershipExpiration);
    } catch (e) {
        console.log("index " + idtyIdenx + " have no expiration of membership");
    return 0;
    }
    return obj['expireOn']
}

async function getSessionExpirationJS(idtyIdenx) {
    const sessionExpiration = (await api.query.authorityMembers.members(idtyIdenx.toString())).toString();
    
    try {
        obj = JSON.parse(sessionExpiration);
    } catch (e) {
        console.log("index " + idtyIdenx + " have no session expiration");
        return [0,0];
    }
    return [+obj['expireOnSession'], +obj['mustRotateKeysBefore']];
}

//counter of smith certs received and issued (will be parsed List int)
async function getSmithCertsJS(idtyIndex) {
    const _smithCertsReceiver = (await api.query.smithCert.storageIdtyCertMeta(idtyIndex.toString())).toString();
    const obj = JSON.parse(_smithCertsReceiver);

    return [+obj['receivedCount'], +obj['issuedCount']];
}

//Smith Membership Expiration (will be parsed int)
async function getSmithMembershipExpirationJS(idtyIdenx) {
    var smithMembershipExpiration = await api.query.smithMembership.membership(idtyIdenx.toString())
    
    smithMembershipExpiration = smithMembershipExpiration.toString();

    try {
        obj = JSON.parse(smithMembershipExpiration);
    } catch (e) {
        console.log("index " + idtyIdenx + " have no expiration of smith membership");
        return 0;
    }
    //if(obj.hasOwnProperty('expireOn'))
    return obj['expireOn']
}

// We use app JS object directly to test this way
async function getCurrentSessionJS() {
    return await api.query.session.currentIndex()
}

async function getCurrentTimeJS() {
    return await api.query.timestamp.now()
}

///////////////////////////////
//////// SUBSCRIPTIONS ////////
///////////////////////////////

async function subscribeNewSessionsJS(callback) {
    return await api.query.session.currentIndex((sessionNum) => callback(sessionNum))
}

const String latestTransactionsPagedQ = r'''
query ($cursor: String, $number: Int!)  {
  transferConnection(orderBy: {timestamp: DESC}, first: $number, after: $cursor) {
    pageInfo {
      endCursor
      hasNextPage
    }
    edges {
      node {
        amount
        fromId
        timestamp
        from {
          identity {
            name
          }
        }
        toId
        to {
          identity {
            name
          }
        }
      }
    }
  }
}
''';

const String latestIdentityPagedQ = r'''
query ($after: String, $before: String, $number: Int, $cursor: String, $orderBy: [IdentityOrderBy!], $where: IdentityBoolExp) {
  identityConnection(
    after: $cursor
    first: $number
    orderBy: {createdOn: DESC}
  ) {
    edges {
      node {
        accountId
        name
        createdIn {
          block {
            timestamp
          }
        }
      }
    }
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
''';

const String getNameByAddressQ = r'''
query ($name: String!)  {
  accountByPk(id: $name) {
    identity {
      name
      accountId
    }
  }
}
''';

const String searchAddressByNameQ = r'''
query ($name: String!) {
  identity(where: {name: {_ilike: $name}}, orderBy: {name: ASC}, limit: 10) {
    name
    accountId
  }
}
''';

const String getHistoryByAddressQ = r'''
query ($address: String!, $number: Int!, $cursor: String) {
  transferConnection(
    orderBy: {timestamp: DESC}
    first: $number,
    after: $cursor,
    where: {
      _or: [
        {toId: {_eq: $address}},
        {fromId: {_eq: $address}}
      ]
    }
  ) {
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
    }
    edges {
      node {
        fromId
        timestamp
        from {
          identity {
            name
          }
        }
        toId
        to {
          identity {
            name
          }
        }
        amount
      }
    }
  }
}

''';

const String getListNameByAddressQ = r'''
query ($listPubKey: [String!]) {
  account(where: {id: {_in: $listPubKey}}) {
    id
    identity {
      name
    }
  }
}
''';

const String getBlockchainStartQ = r'''
query {
  block(where: {height: {_eq: 0}}) {
    events {
      block {
        timestamp
      }
    }
  }
}
''';

const String getCertsReceived = r'''
query ($address: String) {
  cert(
    where: {receiver: {accountId: {_eq: $address}}}
    orderBy: {createdIn: {block: {timestamp: DESC}}}
  ) {
    issuer {
      accountId
      name
      createdIn {
        block {
          timestamp
        }
      }
    }
  }
}
''';

const String getCertsSent = r'''
query ($address: String) {
  cert(
    where: {issuer: {accountId: {_eq: $address}}}
    orderBy: {createdIn: {block: {timestamp: DESC}}}
  ) {
    receiver {
      accountId
      name
      createdIn {
        block {
          timestamp
        }
      }
    }
  }
}
''';

const String getSmithCertsReceived = r'''
query ($address: String) {
  smith_cert(where: {receiver: {identity: {pubkey: {_eq: $address}}}}) {
    issuer {
      identity {
        index
        name
        pubkey
        validated_at
      }
    }
  }
}
''';

const String getSmithCertsSent = r'''
query ($address: String) {
  smith_cert(where: {issuer: {identity: {pubkey: {_eq: $address}}}}) {
    receiver {
      identity {
        pubkey
        name
        validated_at
        index
      }
    }
  }
}
''';

const String getSmithData = r'''
query {
  smith {
    idty_index
    identity {
      pubkey
      name
    }
  }
}
''';

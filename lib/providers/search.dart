import 'package:flutter/material.dart';

class SearchProvider with ChangeNotifier {
  final searchController = TextEditingController();
  List searchResult = [];
  bool isLoading = false;
  final searchFocus = FocusNode();


  void reload() {
    notifyListeners();
  }
}

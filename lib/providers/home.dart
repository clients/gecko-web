// ignore_for_file: use_build_context_synchronously

import 'dart:async';
import 'dart:convert';
import 'package:auto_route/auto_route.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/activity.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/search.dart';
import 'package:gecko_web/utils.dart';
import 'package:gecko_web/widgets/balance.dart';
import 'package:gecko_web/widgets/loading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:truncate/truncate.dart';

class HomeProvider with ChangeNotifier {
  String currentAddress = '';
  String currentName = '';
  final payAmount = TextEditingController();
  final mnemonicController = TextEditingController();
  late String browserName;

  // gql pagination
  int nRepositories = 20;
  List<Widget> parsedTransactionsData = [];
  List parsedIndentityData = [];
  Map<String, List> parsedHistoryData = {};
  Widget indexerEndpointStatus = const Icon(Icons.close, color: Colors.redAccent);

  bool isLargeScreen = false;

  // router
  bool showCertsView = false;

  Future<String> getValidIndexerEndpoint([String? selectedIndexerEndpoint]) async {
    final polka = Provider.of<PolkadotProvider>(homeContext, listen: false);
    indexerEndpointStatus = const Loading(size: 24);
    polka.reload();
    listIndexerEndpoints = await rootBundle.loadString('config/indexer_endpoints.json').then((jsonStr) => jsonDecode(jsonStr));
    // listEndpoints.shuffle();

    if (selectedIndexerEndpoint != null) {
      final isValid = await checkIndexerEndpoint(selectedIndexerEndpoint);
      currentIndexerEndpoint = selectedIndexerEndpoint;
      if (isValid) {
        indexerEndpointStatus = const Icon(Icons.check, color: Colors.green);
      } else {
        indexerEndpointStatus = const Icon(Icons.close, color: Colors.redAccent);
      }
      polka.reload();
      return selectedIndexerEndpoint;
    }

    for (final String testIndexerEndpoint in listIndexerEndpoints!) {
      final isValid = await checkIndexerEndpoint(testIndexerEndpoint);
      if (isValid) {
        currentIndexerEndpoint = testIndexerEndpoint;
        indexerEndpointStatus = const Icon(Icons.check, color: Colors.green);
        break;
      }
      indexerEndpointStatus = const Icon(Icons.close, color: Colors.redAccent);
    }
    polka.reload();

    return currentIndexerEndpoint!;
  }

  Future<String> getAppVersion() async {
    String version;
    String buildNumber;
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    version = packageInfo.version;
    buildNumber = packageInfo.buildNumber;

    notifyListeners();
    return 'v$version+$buildNumber';
  }

  Future closeProfile() async {
    final searchProvider = Provider.of<SearchProvider>(homeContext, listen: false);
    searchProvider.searchController.text = '';
    currentAddress = '';
    currentName = '';
    homeContext.router.replaceNamed('/');
    homeContext.router.navigationHistory.rebuildUrl();
    while (homeContext.router.urlState.path != '/') {
      await Future.delayed(const Duration(milliseconds: 20));
    }
    notifyListeners();
    searchProvider.reload();
    searchProvider.searchFocus.requestFocus();
  }

  void backProfile() async {
    final oldState = homeContext.router.urlState;
    homeContext.router.back();
    while (homeContext.router.urlState == oldState) {
      await Future.delayed(const Duration(milliseconds: 20));
    }
    applyPath();
    notifyListeners();
  }

  // void forwardProfile() async {
  //   homeContext.router.navigationHistory.forward();
  //   await Future.delayed(const Duration(milliseconds: 10));
  //   applyPath();
  //   notifyListeners();
  // }

  void applyPath() async {
    final currentUrl = homeContext.router.currentUrl.split('/');
    currentUrl.remove('');
    // log.d(currentUrl);
    switch (currentUrl.first) {
      case 'profile':
        if (currentUrl.length < 2) {
          log.i('no selected profile, go home');
          homeContext.router.replaceNamed('/');
          homeContext.router.navigationHistory.rebuildUrl();
        } else if (isAddress(currentUrl[1])) {
          currentAddress = currentUrl[1];
          currentName = '';
          if (screenWidth < screenBreakpoint) {
            while (scaffoldKey.currentState == null) {
              await Future.delayed(const Duration(milliseconds: 100));
            }
            scaffoldKey.currentState!.openEndDrawer();
          }
        } else {
          log.i('bad profile, go home');
          homeContext.router.replaceNamed('/');
          homeContext.router.navigationHistory.rebuildUrl();
        }
        break;
      case '':
        if (screenWidth < screenBreakpoint) {
          while (scaffoldKey.currentState == null) {
            await Future.delayed(const Duration(milliseconds: 100));
          }
          scaffoldKey.currentState!.closeEndDrawer();
        }
        currentAddress = '';
        break;
    }
  }

  List<Widget> parseLatestTransactions(BuildContext context, List resultQ, Map pageInfo) {
    List<Widget> transactionsList = [];

    for (final transactionNode in resultQ) {
      final transaction = transactionNode['node'];
      final String issuerAddress = transaction['fromId'];
      final String issuerName = transaction['from']['identity']?['name'] ?? '';
      final String receiverAddress = transaction['toId'];
      final String receiverName = transaction['to']['identity']?['name'] ?? '';
      final int amount = transaction['amount'];
      final DateTime date = DateTime.parse(transaction['timestamp']).toLocal();
      transactionsList.add(rowTransaction(context, issuerAddress, issuerName, receiverAddress, receiverName, amount, date));
    }

    return transactionsList;
  }

  Widget rowTransaction(BuildContext context, String issuerAddress, String issuerName, String receiverAddress, String receiverName, int amount, DateTime date) {
    final double humainAmount = amount / 100;
    final String year = truncate("${date.year}", 2, omission: "", position: TruncatePosition.start);
    final month = monthsInYear[date.month];
    final String humainDate = "${date.day} $month $year";
    final String hour = "${date.hour}".padLeft(2, '0');
    final String minute = "${date.minute}".padLeft(2, '0');
    final String humainHour = "$hour:$minute";

    return SizedBox(
      height: 45,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: 100,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(humainDate, style: const TextStyle(fontWeight: FontWeight.w500)),
                const SizedBox(height: 2),
                Text(humainHour, style: TextStyle(color: Theme.of(context).textTheme.titleLarge!.color)),
              ],
            ),
          ),
          Builder(
            builder: (context) => SizedBox(
              width: 120,
              child: InkWell(
                onTap: () {
                  currentAddress = issuerAddress;
                  currentName = issuerName;

                  notifyListeners();
                  if (screenWidth < screenBreakpoint) {
                    Scaffold.of(context).openEndDrawer();
                  }
                },
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text(
                    getShortPubkey(issuerAddress),
                    style: GoogleFonts.robotoMono(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  const SizedBox(height: 2),
                  Text(
                    issuerName,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Theme.of(context).textTheme.titleLarge!.color),
                  ),
                ]),
              ),
            ),
          ),
          const Icon(Icons.arrow_forward, size: 18),
          const SizedBox(width: 0),
          Builder(
            builder: (context) => SizedBox(
              width: 120,
              child: InkWell(
                onTap: () {
                  currentAddress = receiverAddress;
                  currentName = receiverName;
                  notifyListeners();
                  if (screenWidth < screenBreakpoint) {
                    Scaffold.of(context).openEndDrawer();
                  }
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      getShortPubkey(receiverAddress),
                      style: GoogleFonts.robotoMono(
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(height: 2),
                    Text(
                      receiverName,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Theme.of(context).textTheme.titleLarge!.color),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            width: 100,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [Text('$humainAmount $currencyName', style: TextStyle(color: Theme.of(context).textTheme.titleLarge!.color))]),
          )
        ],
      ),
    );
  }

  List<Widget> parseLatestIdentity(BuildContext context, List resultQ) {
    List<Widget> transactionsList = [];

    for (final transactionNode in resultQ) {
      final transaction = transactionNode['node'];
      final String? address = transaction['accountId'];
      final String name = transaction['name'] ?? '';
      final DateTime date = DateTime.parse(transaction['createdIn']['block']['timestamp'] ?? "1970-01-01T00:00:00+00:00").toLocal();
      if (address == null) continue;
      transactionsList.add(rowIdentity(context, address, name, date));
    }

    return transactionsList;
  }

  Widget rowIdentity(BuildContext context, String address, String name, DateTime date) {
    // SearchProvider search = Provider.of<SearchProvider>(context, listen: false);
    final String year = truncate("${date.year}", 2, omission: "", position: TruncatePosition.start);
    final month = monthsInYear[date.month];
    final String humainDate = "${date.day} $month $year";

    // final String hour = "${date.hour}".padLeft(2, '0');
    // final String minute = "${date.minute}".padLeft(2, '0');
    // final String humainHour = "$hour:$minute";

    return SizedBox(
      height: 46,
      child: Builder(
        builder: (context) => InkWell(
          onTap: () {
            currentAddress = address;
            currentName = name;
            notifyListeners();
            if (screenWidth < 1130) {
              Scaffold.of(context).openEndDrawer();
            }
          },
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Column(
              children: [
                SizedBox(
                  width: 100,
                  child: Text(humainDate, style: const TextStyle(fontWeight: FontWeight.w500)),
                ),
                // Text(
                //   humainHour,
                //   style: TextStyle(
                //       fontWeight: FontWeight.w100,
                //       color: Colors.grey[400],
                //       fontSize: 14),
                // ),
              ],
            ),
            SizedBox(
              width: 150,
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text(
                  name,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 17),
                ),
                Text(
                  getShortPubkey(address),
                  style: GoogleFonts.robotoMono(color: Theme.of(context).textTheme.titleLarge!.color),
                ),
                const SizedBox(height: 2),
              ]),
            ),
            const SizedBox(width: 0),
            SizedBox(
              width: 120,
              child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                Balance(address: address, size: 14, color: Theme.of(context).textTheme.titleLarge!.color!),
              ]),
            )
          ]),
        ),
      ),
    );
  }

  InlineSpan makeTooltip(BuildContext context, String address, String name) {
    return WidgetSpan(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            key: const Key('copyPubkey'),
            onTap: () {
              Clipboard.setData(ClipboardData(text: address));
              snackCopyKey(context, address);
            },
            child: Text(
              getShortPubkey(address),
              style: const TextStyle(fontWeight: FontWeight.w600),
            ),
          ),
          const SizedBox(height: 2),
          Text(
            name,
            style: TextStyle(color: Theme.of(context).textTheme.titleLarge!.color),
          ),
        ],
      ),
    );
  }

  snackCopyKey(BuildContext context, String address) {
    Clipboard.setData(ClipboardData(text: address));

    final snackBar = SnackBar(
        padding: const EdgeInsets.all(20),
        content: Text("thisAddressHasBeenCopiedToClipboard".tr(), style: const TextStyle(fontSize: 16)),
        duration: const Duration(seconds: 2));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Future asyncStartup([String? selectedDuniterEndpoint]) async {
    final polka = Provider.of<PolkadotProvider>(homeContext, listen: false);

    await getBrowserInfo();

    // Connect to Duniter indexer
    await getValidIndexerEndpoint();

    // Connect to Duniter node
    listDuniterEndpoints = await getValidEndpoints();
    final List endpointsToConnect = selectedDuniterEndpoint != null ? [selectedDuniterEndpoint] : listDuniterEndpoints!;
    await polka.connectNode(endpointsToConnect);
    await polka.getInjectedAccounts();
    // notifyListeners();
  }

  Future<List> getValidEndpoints() async {
    listDuniterEndpoints = await rootBundle.loadString('config/gdev_endpoints.json').then((jsonStr) => jsonDecode(jsonStr));
    listDuniterEndpoints!.shuffle();

    log.i('ENDPOINT: $listDuniterEndpoints');
    return listDuniterEndpoints!;
  }

  Future getBrowserInfo() async {
    // Get browser name
    final deviceInfo = DeviceInfoPlugin();
    final browserInfo = await deviceInfo.webBrowserInfo;
    browserName = browserInfo.browserName.name;
  }

  void reload() {
    notifyListeners();
  }

  FetchMoreOptions mergeQueryResult(BuildContext context, String req, QueryResult result, {String? address}) {
    final activityProvider = Provider.of<ActivityProvider>(context, listen: false);

    List<dynamic> resultData = result.data![req]['edges'];

    final Map pageInfo = result.data![req]['pageInfo'];
    final String fetchMoreCursor = pageInfo['endCursor'];

    final opts = FetchMoreOptions(
      variables: {'cursor': fetchMoreCursor, 'number': nRepositories},
      updateQuery: (previousResultData, fetchMoreResultData) {
        // final List<dynamic> repos = [
        //   ...previousResultData![req]['edges'] as List<dynamic>,
        //   ...fetchMoreResultData![req]['edges'] as List<dynamic>
        // ];

        // fetchMoreResultData[req]['edges'] = repos;

        return fetchMoreResultData;
      },
    );

    log.d("###### DEBUG pagination $req - Cursor: $fetchMoreCursor ######");
    int resultToRemove = 0;
    if (resultData.length > nRepositories) {
      resultToRemove = resultData.length - nRepositories;
    }

    if (!(pageInfo['hasPreviousPage'] ?? true)) {
      if (req == 'transferConnection') parsedHistoryData = {};
      if (req == 'identityConnection') parsedIndentityData.clear();
    }

    switch (req) {
      case 'transferConnection':
        if (address == null) {
          parsedTransactionsData += parseLatestTransactions(context, resultData.sublist(resultToRemove), pageInfo);
        } else {
          parsedHistoryData[address] = (parsedHistoryData[address] ?? []) + activityProvider.parseHistory(resultData.sublist(resultToRemove), address);
        }
        break;
      case 'identityConnection':
        parsedIndentityData += parseLatestIdentity(context, resultData.sublist(resultToRemove));
        break;
    }
    return opts;
  }

  Future<bool> checkIndexerEndpoint(String endpoint) async {
    try {
      final wsLink = WebSocketLink("$endpoint/v1/graphql");
      final GraphQLClient client = GraphQLClient(
        cache: GraphQLCache(),
        link: wsLink,
      );

      const String testQuery = '''
      query {
        __typename
      }
    ''';

      final QueryResult result = await client
          .query(
            QueryOptions(
              document: gql(testQuery),
              fetchPolicy: FetchPolicy.noCache,
            ),
          )
          .timeout(const Duration(seconds: 2));

      if (result.hasException) {
        log.d('Indexer $endpoint est hors ligne : ${result.exception.toString()}');
        currentIndexerEndpoint = '';
        notifyListeners();
        return false;
      } else {
        currentIndexerEndpoint = endpoint;
        log.d('Indexer $endpoint est en ligne !');
        notifyListeners();
        return true;
      }
    } catch (e) {
      log.d('Indexer $endpoint est hors ligne : ${e.toString()}');
      currentIndexerEndpoint = '';
      notifyListeners();
      return false;
    }
  }
}

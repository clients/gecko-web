// ignore_for_file: avoid_web_libraries_in_flutter

import 'dart:convert';
import 'dart:js_util';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/models/wallet_data.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/indexer.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/polkadot_lib.dart';
import 'package:provider/provider.dart';

class Smiths with ChangeNotifier {
  final indexer = Indexer();
  int currentSession = 0;
  int nbrIdentity = 0;
  int nbrMembers = 0;
  int nbrAutorities = 0;
  List<SmithData> listSmiths = [];
  bool isSmithStartupLoaded = false;
  bool showCertsView = false;
  bool isSmithCerts = false;
  late SmithData currentSmithDetails;
  bool showForm = true;

  Future asyncStartup(BuildContext context) async {
    // Connect to duniter node using javascript interface
    // final polkaSubBlock = PolkadotSubscribBlocksProvider();
    final polka = Provider.of<PolkadotProvider>(context, listen: false);
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);

    // subscribreBalance('5CQ8T4qpbYJq7uVsxGPQ5q2df7x3Wa4aRY6HUWMBYjfLZhnn');

    if (!polka.nodeConnected) {
      await homeProvider.asyncStartup();
    }

    await getSmithMembershipCounter();
    await getAuthoritiesCounter();
    //await getCurrentSession();

    // Init hive database
    // We're using HiveStore for persistence
    // await initHiveForFlutter();

    // Get Smiths
    //await getSmithsHardcoded();
    //await getSmiths();
    listSmiths = await indexer.listSmiths();
    // listSmiths.add(SmithData(
    //     name: "Guenoel",
    //     address: "5H1oBx5VnURR4cS536Hz1DQwfbkGmXDtQ4rcspMkHmyUAEqA",
    //     idtyIndex: 2308));
    await getSmithDetails();
    if (polka.listMyWallets.isEmpty) {
      showForm = false;
    }
    for (var wallet in polka.listMyWallets.values) {
      if (listSmiths.any((smith) => smith.address == wallet.address)) {
        showForm = false;
      }
    }
    if (polka.listMyWallets.values
        .every((wallet) => wallet.status == IdentityStatus.member)) {
      showForm = false;
    }

    isSmithStartupLoaded = true;
    notifyListeners();
  }

  //Sum of membership
  Future<int> getSmithMembershipCounter() async {
    final promise = getSmithMembershipCounterJS();
    nbrMembers = int.parse(await promiseToFuture(promise));
    log.d(nbrMembers);
    return nbrMembers;
  }

  //sum of Authorities
  Future<int> getAuthoritiesCounter() async {
    final promise = getAuthoritiesCounterJS();
    nbrAutorities = int.parse(await promiseToFuture(promise));
    return nbrAutorities;
  }

  Future<int> getCurrentSession() async {
    final currentSessionJS = await promiseToFuture(getCurrentSessionJS());
    currentSession = int.parse(
        currentSessionJS.toString() == '' ? '0' : currentSessionJS.toString());
    notifyListeners();
    return currentSession;
  }

  Future<int> getReceivedCerts(int smithIndex) async {
    final promise = getSmithCertsJS(smithIndex);
    final smithCertsJS = (await promiseToFuture(promise));
    final smithCert = int.parse(smithCertsJS[0].toString());

    return smithCert;
  }

  Future<int> getSmithMembershipExpirationFromIndex(int smithIndex) async {
    final promise2 = getSmithMembershipExpirationJS(smithIndex);
    final membershipExpirationJS = (await promiseToFuture(promise2));
    final membershipExpiration = (int.parse(
        membershipExpirationJS.toString() == ''
            ? '0'
            : membershipExpirationJS.toString()));
    return membershipExpiration;
  }

  Future getSmithDetails() async {
    SmithStatus status;
    SmithData smith;

    for (smith in listSmiths) {
      final onlineSmiths = await getListIndexes(getOnlineSmithsJS());
      final incomingAuthorities =
          await getListIndexes(getIncomingAuthoritiesJS());
      final outgoingAuthorities =
          await getListIndexes(getOutgoingAuthoritiesJS());
      final certs = await getReceivedCerts(smith.idtyIndex);
      final smithCertifBlockExpir =
          await getSmithMembershipExpirationFromIndex(smith.idtyIndex);
      final memberCertifBlockExpir =
          await getMembershipExpirationFromIndex(smith.idtyIndex);
      final sessionKeyExpir = await getSessionExpiration(smith.idtyIndex);

      if (incomingAuthorities.contains(smith.idtyIndex)) {
        status = SmithStatus.incoming;
      } else if (outgoingAuthorities.contains(smith.idtyIndex)) {
        status = SmithStatus.outgoing;
      } else if (onlineSmiths.contains(smith.idtyIndex)) {
        status = SmithStatus.online;
      } else if (certs < 3) {
        status = SmithStatus.requested;
      } else if (smithCertifBlockExpir == 0) {
        status = SmithStatus.certified;
      } else {
        status = SmithStatus.offline;
      }
      smith.status = status;
      smith.smithCertifBlockExpir = smithCertifBlockExpir;
      smith.memberCertifBlockExpir = memberCertifBlockExpir;
      smith.sessionKeyExpir = sessionKeyExpir[0];
      smith.mustRotateKeys = sessionKeyExpir[1];
    }
  }

  //get list of indexes from JS function
  Future<List<int>> getListIndexes(promiseListIndexesJS) async {
    final List listIndexesJS = await promiseToFuture(promiseListIndexesJS);
    // if (listIndexesJS.isEmpty) return [];
    if (listIndexesJS.toString() == '[]') return [];
    final List<int> listIndexes = [];

    // //TODO: remove this fucking toto trick needed for production mode...
    final toto = listIndexesJS
        .toString()
        .replaceAll('[', '')
        .replaceAll(']', '')
        .split(',');

    for (int i = 0; i < toto.length; i++) {
      listIndexes.add(int.parse(toto[i].toString()));
    }

    return listIndexes;
  }

  //get single index from pubkey
  Future<String> getIdentityAddress(int idtyIndex) async {
    final Map? idtyData = idtyIndex == 0
        ? null
        : json.decode(
            (await promiseToFuture(getIdentityDataJS(idtyIndex))).toString());
    return idtyData?['ownerKey'] ?? "";
  }

  Future<int> getMembershipExpiration(SmithData smith) async {
    final promise = getMembershipExpirationJS(smith.idtyIndex);
    final membershipExpirationJS = (await promiseToFuture(promise));
    final membershipExpiration = (int.parse(
        membershipExpirationJS.toString() == ''
            ? '0'
            : membershipExpirationJS.toString()));
    return membershipExpiration;
  }

  Future<int> getMembershipExpirationFromIndex(int idtyIndex) async {
    final promise = getMembershipExpirationJS(idtyIndex);
    final membershipExpirationJS = (await promiseToFuture(promise));
    final membershipExpiration = (int.parse(
        membershipExpirationJS.toString() == ''
            ? '0'
            : membershipExpirationJS.toString()));
    return membershipExpiration;
  }

  Future<int> getSmithMembershipExpiration(SmithData smith) async {
    final promise2 = getSmithMembershipExpirationJS(smith.idtyIndex);
    final smithMembershipExpirationJS = (await promiseToFuture(promise2));
    final smithMembershipExpiration = int.parse(
        smithMembershipExpirationJS.toString() == ''
            ? '0'
            : smithMembershipExpirationJS.toString());
    return smithMembershipExpiration;
  }

  Future<List<int>> getSessionExpiration(int idtyIndex) async {
    final promise = getSessionExpirationJS(idtyIndex);
    final sessionExpirationJS = (await promiseToFuture(promise));
    final sessionExpiration = [
      int.parse(sessionExpirationJS[0].toString()),
      int.parse(sessionExpirationJS[1].toString())
    ];
    return sessionExpiration;
  }

  void reload() {
    notifyListeners();
  }
}

// ignore_for_file: avoid_web_libraries_in_flutter
import 'dart:js_util';
import 'package:flutter/material.dart';
import 'dart:js' as js;

import 'package:gecko_web/providers/polkadot_lib.dart';
//import 'package:hex/hex.dart';

class PolkadotSubscribBlocksProvider with ChangeNotifier {
  int blockNumber = 0;

  Future subscribeNewBlocs() async {
    var callbackJS = js.allowInterop(newBlocsCallback);
    js.context.callMethod('subscribeNewBlocsJS', [callbackJS]);
  }

  void newBlocsCallback(js.JsObject ev) {
    blockNumber = int.parse(ev['number'].toString());
    notifyListeners();
  }

  Future<int> getCurrentTime() async {
    var promise = getCurrentTimeJS();
    var timeJS = (await promiseToFuture(promise));

    return int.parse(timeJS.toString());
  }
}

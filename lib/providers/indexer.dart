import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/queries.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class Indexer {
  final GraphQLClient client;

  Indexer._(this.client);

  static Indexer? _standardInstance;
  static Indexer? _relayInstance;

  factory Indexer() => standard;

  static Indexer get standard {
    if (_standardInstance == null) {
      final wsLink = WebSocketLink("$currentIndexerEndpoint/v1/graphql");
      final client = GraphQLClient(
        cache: GraphQLCache(),
        link: wsLink,
      );
      _standardInstance = Indexer._(client);
    }
    return _standardInstance!;
  }

  static Indexer get relay {
    if (_relayInstance == null) {
      final wsLink = WebSocketLink("$currentIndexerEndpoint/v1beta1/relay");
      final client = GraphQLClient(
        cache: GraphQLCache(),
        link: wsLink,
      );
      _relayInstance = Indexer._(client);
    }
    return _relayInstance!;
  }

  // Méthode pour réinitialiser les instances (utile si l'endpoint change)
  static void resetInstances() {
    _standardInstance = null;
    _relayInstance = null;
  }

  Future<Map<String, String>> namesByAddress(List<String> addresses) async {
    Map<String, String> mapNames = {};

    final result = await execQuery(getListNameByAddressQ, {'listPubKey': addresses});

    if (result.hasException) {
      log.d(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request');
    } else {
      final List res = result.data?['account'] ?? [];
      for (int i = 0; i < res.length; i++) {
        mapNames.putIfAbsent(res[i]['id']!, () => res[i]['identity']?['name']! ?? '');
      }
    }

    return mapNames;
  }

  Future<QueryResult> execQuery(String query, [Map<String, dynamic> variables = const {}]) async {
    final QueryOptions options = QueryOptions(document: gql(query), variables: variables);

    return await client.query(options);
  }

  Future debugGql() async {
    final result = await execQuery(latestTransactionsPagedQ, {'cursor': null, 'number': 250});

    log.d(result.data!['transferConnection']['edges'].length);
  }

  Future<List<SmithData>> listSmiths() async {
    List<SmithData> listSmiths = [];

    final result = await execQuery(getSmithData);

    if (result.hasException) {
      log.d(currentIndexerEndpoint);
      log.e(result.exception.toString());
      throw Exception('Unexpected error happened in graphql request\n${result.exception.toString()}');
    } else {
      final List res = result.data?['smith_aggregate']?['nodes'] ?? [];
      for (int i = 0; i < res.length; i++) {
        listSmiths.add(SmithData(
          address: res[i]['identity']['accountId'],
          name: res[i]['identity']['name'],
          idtyIndex: res[i]['idty_index'],
        ));
      }
    }
    return listSmiths;
  }
}

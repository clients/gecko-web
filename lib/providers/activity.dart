import 'package:flutter/material.dart';

class ActivityProvider with ChangeNotifier {
  List parseHistory(blockchainTX, address) {
    var transBC = [];
    int i = 0;

    for (final trans in blockchainTX) {
      final transaction = trans['node'];
      final direction =
          transaction['fromId'] != address ? 'RECEIVED' : 'SENT';

      transBC.add(i);
      transBC[i] = [];
      transBC[i].add(DateTime.parse(transaction['timestamp']));
      final int amountBrut = transaction['amount'];
      final num amount = removeDecimalZero(amountBrut / 100);
      if (direction == "RECEIVED") {
        transBC[i].add(transaction['fromId']);
        transBC[i].add(transaction['from']['identity']?['name'] ?? '');
        transBC[i].add(amount.toString());
        transBC[i].add("RECEIVED");
      } else if (direction == "SENT") {
        transBC[i].add(transaction['toId']);
        transBC[i].add(transaction['to']['identity']?['name'] ?? '');
        transBC[i].add('- $amount');
        transBC[i].add("SENT");
      }
      // transBC[i].add(''); //transaction comment

      i++;
    }
    return transBC;
  }

  num removeDecimalZero(double n) {
    String result = n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 2);
    return num.parse(result);
  }
}

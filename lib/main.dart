// ignore_for_file: avoid_print

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/activity.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/pay.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/providers/polkadot_subscribe_sessions.dart';
import 'package:gecko_web/providers/search.dart';
import 'package:gecko_web/providers/smiths_provider.dart';
import 'package:gecko_web/router.dart';
import 'package:gecko_web/providers/my_theme.dart';
// import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  final homeP = HomeProvider();
  appVersion = await homeP.getAppVersion();

  runApp(
    EasyLocalization(
        supportedLocales: const [Locale('en'), Locale('fr'), Locale('es')],
        path: 'assets/translations',
        fallbackLocale: const Locale('en'),
        child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => HomeProvider()),
        ChangeNotifierProvider(create: (_) => MyTheme()),
        ChangeNotifierProvider(create: (_) => SearchProvider()),
        ChangeNotifierProvider(create: (_) => ActivityProvider()),
        ChangeNotifierProvider(create: (_) => PayProvider()),
        ChangeNotifierProvider(create: (_) => PolkadotProvider()),
        ChangeNotifierProvider(create: (_) => Smiths()),
        ChangeNotifierProvider(create: (_) => PolkadotSubscribBlocksProvider()),
        ChangeNotifierProvider(
            create: (_) => PolkadotSubscribSessionsProvider()),
      ],
      child: Consumer<MyTheme>(builder: (context, myTheme, _) {
        return MaterialApp.router(
          localizationsDelegates: context.localizationDelegates,
          supportedLocales: context.supportedLocales,
          locale: context.locale,
          title: 'Ğecko web',
          theme: myTheme.getTheme(),
          routerConfig: _appRouter.config(),
        );
      }),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/widgets/loading.dart';
import 'package:provider/provider.dart';

class Balance extends StatelessWidget {
  const Balance({
    required this.address,
    required this.size,
    this.color = Colors.black,
    this.loadingColor = const Color(0xffd07316),
    super.key,
  });

  final String address;
  final double size;
  final Color color;
  final Color loadingColor;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Consumer2<PolkadotProvider, PolkadotSubscribBlocksProvider>(
          builder: (context, polka, _, __) {
        return FutureBuilder(
            future: polka.getBalance(address),
            builder: (BuildContext context, AsyncSnapshot<Map> balanceBrut) {
              if (balanceBrut.connectionState != ConnectionState.done) {
                if (polka.balanceCache[address] != null &&
                    polka.balanceCache[address] != -1) {
                  return Text(
                      "${polka.balanceCache[address]!.toString()} $currencyName",
                      style: TextStyle(fontSize: size, color: color));
                } else {
                  return const Loading();
                }
              } else if (balanceBrut.hasError) {
                log.d(address);
                log.e(balanceBrut.error);
                return const Icon(
                  Icons.close,
                  color: Colors.redAccent,
                );
              }
              polka.balanceCache[address] =
                  balanceBrut.data!['transferableBalance'];
              if (polka.balanceCache[address] != -1) {
                return Text(
                  "${polka.balanceCache[address]!.toString()} $currencyName",
                  style: TextStyle(
                    fontSize: size,
                    color: color,
                  ),
                );
              } else {
                return const Text('');
              }
            });
      }),
    ]);
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/indexer.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/queries.dart';
import 'package:gecko_web/widgets/loading.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class LatestIdentityWidget extends StatelessWidget {
  const LatestIdentityWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    final scrollController = ScrollController();
    FetchMoreOptions opts;

    return SizedBox(
      height: 327,
      width: 550,
      child: Column(
        children: [
          Row(
            children: [
              Consumer<PolkadotProvider>(builder: (context, polka, _) {
                return Text(
                  ' Liste des identités (${polka.nbrIdentity})',
                  style: TextStyle(fontStyle: FontStyle.italic, color: Theme.of(context).textTheme.titleLarge!.color),
                );
              }),
            ],
          ),
          const SizedBox(height: 7),
          Container(
            height: 300,
            width: 550,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey[500]!,
                width: 1,
              ),
              borderRadius: const BorderRadius.all(Radius.circular(5.0)),
            ),
            child: GraphQLProvider(
              client: ValueNotifier(Indexer.relay.client),
              child: Query(
                  options: QueryOptions(
                    document: gql(latestIdentityPagedQ),
                    variables: const <String, dynamic>{'cursor': null, 'number': 20},
                  ),
                  builder: (QueryResult resultQ, {VoidCallback? refetch, FetchMore? fetchMore}) {
                    if (resultQ.isLoading && resultQ.data == null) {
                      return const Center(child: Loading());
                    }

                    if (resultQ.hasException) {
                      log.d('Error Indexer: ${resultQ.exception}');
                      return Column(children: <Widget>[
                        const SizedBox(height: 50),
                        Text(
                          "noDataToDisplay".tr(),
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontSize: 18),
                        )
                      ]);
                    } else if (resultQ.data?['identityConnection']?['edges'].isEmpty) {
                      return Column(children: <Widget>[
                        const SizedBox(height: 50),
                        Text(
                          "noDataToDisplay".tr(),
                          style: const TextStyle(fontSize: 18),
                        )
                      ]);
                    }

                    final Map pageInfo = resultQ.data!['identityConnection']['pageInfo'];

                    opts = homeProvider.mergeQueryResult(context, 'identityConnection', resultQ);

                    return NotificationListener(
                      child: Builder(
                        builder: (context) => ListView(controller: scrollController, children: <Widget>[
                          for (var identity in homeProvider.parsedIndentityData)
                            Padding(padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5), child: identity),
                          if (resultQ.isLoading)
                            const Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Loading(),
                              ],
                            ),
                        ]),
                      ),
                      onNotification: (dynamic t) {
                        if (t is ScrollEndNotification &&
                            scrollController.position.pixels >= scrollController.position.maxScrollExtent * 0.7 &&
                            pageInfo['hasNextPage'] &&
                            resultQ.isNotLoading) {
                          fetchMore!(opts);
                        }
                        return true;
                      },
                    );
                  }),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:provider/provider.dart';

class MainMenu extends StatelessWidget {
  const MainMenu({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        EntryMenu(name: 'home', route: '/'),
        // EntryMenu(name: 'smiths', route: '/smiths'),
      ],
    );
  }
}

class EntryMenu extends StatelessWidget {
  const EntryMenu({
    super.key,
    required this.name,
    required this.route,
  });

  final String name;
  final String route;

  @override
  Widget build(BuildContext context) {
    final router = context.router;
    final isActualRoute = router.current.path == route;
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);

    routeToGo() {
      if (isActualRoute) {
        homeProvider.currentAddress = '';
        context.router.replaceNamed(route);
        context.router.navigationHistory.rebuildUrl();
        homeProvider.reload();
        return null;
      } else {
        return router.pushNamed(route);
      }
    }

    return Padding(
      padding: const EdgeInsets.only(bottom: 5),
      child: InkWell(
        onTap: () => routeToGo(),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 60, vertical: 10),
          child: SizedBox(
            width: 70,
            child: Center(
              child: Text(
                name,
                style: TextStyle(
                    fontSize: isActualRoute ? 20 : 18,
                    color: isActualRoute
                        ? Theme.of(context).textTheme.bodyLarge!.color
                        : Theme.of(context).textTheme.titleLarge!.color,
                    fontWeight:
                        isActualRoute ? FontWeight.w700 : FontWeight.w100),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

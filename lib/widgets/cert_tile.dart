import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/utils.dart';
import 'package:provider/provider.dart';

class CertTile extends StatelessWidget {
  const CertTile({
    super.key,
    required this.listCerts,
  });

  final List listCerts;

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    const double avatarSize = 200;

    return Column(
        children: listCerts.map((repository) {
      return Column(children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 0),
          child: ListTile(
              contentPadding:
                  const EdgeInsets.only(left: 15, right: 20, top: 8, bottom: 8),
              leading: ClipOval(
                child: defaultAvatar(avatarSize),
              ),
              title: Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(repository['name'],
                    style: const TextStyle(fontSize: 16)),
              ),
              subtitle: RichText(
                text: TextSpan(
                  style: TextStyle(
                    fontSize: 14,
                    color: Theme.of(context).textTheme.titleLarge!.color,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: repository['date'],
                    ),
                    if (repository[2] != '')
                      TextSpan(
                        text: '  ·  ',
                        style: TextStyle(
                          fontSize: 18,
                          color: Theme.of(context).textTheme.titleLarge!.color,
                        ),
                      ),
                    TextSpan(
                      text: getShortPubkey(repository['address']),
                      style: TextStyle(
                          fontStyle: FontStyle.italic,
                          color: Theme.of(context).textTheme.titleLarge!.color),
                    ),
                  ],
                ),
              ),
              dense: false,
              isThreeLine: false,
              onTap: () {
                homeProvider.currentAddress = repository['address'];
                homeProvider.currentName = repository['name'];
                homeProvider.reload();
                context.router.popUntilRoot();
                if (screenWidth < screenBreakpoint) {
                  Scaffold.of(context).openEndDrawer();
                }
              }),
        ),
      ]);
    }).toList());
  }
}

Widget defaultAvatar(double size) => ClipOval(
      child: Image.asset(('assets/icon_user.png'), height: size),
    );

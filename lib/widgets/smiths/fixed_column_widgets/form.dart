// Create a Form widget.
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:provider/provider.dart';

class MyCustomForm extends StatefulWidget {
  final bool enabled;

  const MyCustomForm({
    super.key,
    required this.enabled,
  });

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  late bool enabled;

  @override
  void initState() {
    super.initState();
    enabled = widget.enabled;
  }

  @override
  Widget build(BuildContext context) {
    late VoidCallback? onPressed = enabled
        ? () {
            // Validate returns true if the form is valid, or false otherwise.
            if (_formKey.currentState!.validate()) {
              // If the form is valid, display a snackbar. In the real world,
              // you'd often call a server or save the information in a database.
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('Processing Data')),
              );
            }
          }
        : null;
    // TODO: voir quel consumer pour le enable du bouton
    return Consumer<PolkadotProvider>(builder: (context, polka, _) {
      // Build a Form widget using the _formKey created above.
      return Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const Text('Endpoint:'),
              TextFormField(
                // The validator receives the text that the user has entered.
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 2.0),
                  ),
                  filled: true,
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter endpoint';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 5),
              const Text('Session keys:'),
              TextFormField(
                // The validator receives the text that the user has entered.
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 2.0),
                  ),
                  filled: true,
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter session keys';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 5),
              FilledButton(
                style: enabled
                    ? FilledButton.styleFrom(backgroundColor: Colors.blue)
                    : null,
                onPressed: () {
                  onPressed;
                },
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 8,
                      child: Text(
                        "requestMembership".tr(),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    const Expanded(
                      flex: 2,
                      child: Icon(
                        Icons.arrow_upward,
                        size: 30,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ));
    });
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/providers/polkadot_subscribe_sessions.dart';
import 'package:gecko_web/providers/smiths_provider.dart';
import 'package:gecko_web/utils.dart';
import 'package:provider/provider.dart';

class SmithTime extends StatelessWidget {
  const SmithTime({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Consumer4<Smiths, PolkadotSubscribBlocksProvider,
        PolkadotSubscribSessionsProvider, PolkadotProvider>(
      builder: (context, smithsP, polkaSubBlock, polkaSubSession, polka, _) {
        return Column(
          //crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              width: 400,
              padding: const EdgeInsets.fromLTRB(20, 10, 10, 10),
              decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.0,
                    color: Colors.grey,
                  ),
                  color: Theme.of(context).primaryColorDark,
                  borderRadius: const BorderRadius.all(Radius.circular(20))),
              child: Center(
                child: !smithsP.isSmithStartupLoaded
                    ? Text(
                        "loading".tr(),
                        style: mainStyle,
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                  flex: 6,
                                  child: Text(
                                      "${'currentTimeFromTimestamp'.tr()}: ")),
                              FutureBuilder(
                                  future: polkaSubBlock.getCurrentTime(),
                                  builder:
                                      (context, AsyncSnapshot<int> snapshot) {
                                    return Expanded(
                                      flex: 4,
                                      child: Text(
                                          (DateFormat('d MMM yyyy HH:mm:ss')
                                              .format(
                                        DateTime.fromMillisecondsSinceEpoch(
                                            snapshot.data!),
                                      )).toString()),
                                    );
                                  }),
                            ],
                          ),
                        ],
                      ),
              ),
            ),
            const SizedBox(height: 10),
            Container(
              width: 400,
              padding: const EdgeInsets.fromLTRB(20, 10, 10, 10),
              decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.0,
                    color: Colors.grey,
                  ),
                  color: Theme.of(context).primaryColorDark,
                  borderRadius: const BorderRadius.all(Radius.circular(20))),
              child: Center(
                child: !smithsP.isSmithStartupLoaded
                    ? Text(
                        "loading".tr(),
                        style: mainStyle,
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                  flex: 6,
                                  child: Text(
                                      "${'currentTimeFromBlocks'.tr()}: ")),
                              Expanded(
                                flex: 4,
                                child: Text(
                                    (DateFormat('d MMM yyyy HH:mm:ss').format(
                                  blocNumberToDate(polkaSubBlock.blockNumber),
                                )).toString()),
                              ),
                            ],
                          ),
                          const SizedBox(height: 5),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                  flex: 6,
                                  child: Text("${'currentBlock'.tr()}: ")),
                              Expanded(
                                  flex: 4,
                                  child: Text("${polkaSubBlock.blockNumber}")),
                            ],
                          ),
                        ],
                      ),
              ),
            ),
            const SizedBox(height: 10),
            Container(
              width: 400,
              padding: const EdgeInsets.fromLTRB(20, 10, 10, 10),
              decoration: BoxDecoration(
                  border: Border.all(
                    width: 1.0,
                    color: Colors.grey,
                  ),
                  color: Theme.of(context).primaryColorDark,
                  borderRadius: const BorderRadius.all(Radius.circular(20))),
              child: !smithsP.isSmithStartupLoaded
                  ? Text(
                      "loading".tr(),
                      style: mainStyle,
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              flex: 6,
                              child: Text(
                                "${'currentTimeFromSessions'.tr()}: ",
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Text(
                                  (DateFormat('d MMM yyyy HH:mm:ss').format(
                                sessionNumberToDate(
                                    polkaSubSession.sessionNumber),
                              )).toString()),
                            ),
                          ],
                        ),
                        const SizedBox(height: 5),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                flex: 6,
                                child: Text("${'currentSession'.tr()}: ")),
                            Expanded(
                                flex: 4,
                                child:
                                    Text("${polkaSubSession.sessionNumber}")),
                          ],
                        ),
                      ],
                    ),
            ),
          ],
        );
      },
    );
  }
}

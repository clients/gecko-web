import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/smiths_provider.dart';
import 'package:gecko_web/widgets/my_wallets_list.dart';
import 'package:gecko_web/widgets/smiths/fixed_column_widgets/form.dart';
import 'package:gecko_web/widgets/smiths/fixed_column_widgets/time.dart';
import 'package:provider/provider.dart';

class SmithFixedColumn extends StatefulWidget {
  final bool enabled;

  const SmithFixedColumn({super.key, required this.enabled});

  @override
  FixedColumnState createState() => FixedColumnState();
}

class FixedColumnState extends State<SmithFixedColumn> {
  @override
  Widget build(BuildContext context) {
    final smithP = Provider.of<Smiths>(context, listen: false);

    return Positioned(
      right: 10,
      //left: 10,
      top: 10,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SmithTime(),
          const SizedBox(
            height: 10,
          ),
          ElevatedButton(
            onPressed: () => setState(() => smithP.showForm = !smithP.showForm),
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              fixedSize: const Size(250, 30),
              backgroundColor: orangeC,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "request".tr(),
                  style: const TextStyle(
                    fontSize: 19,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          if (smithP.showForm)
            Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                        width: 1.0,
                        color: Colors.grey,
                      ),
                      color: Theme.of(context).primaryColorDark,
                      borderRadius: const BorderRadius.all(Radius.circular(5))),
                  width: 400,
                  height: 250,
                  padding: const EdgeInsets.all(10),
                  child: const MyCustomForm(
                    //if membre g1 dans la liste de portefeuilles connectés alors true sinon false
                    enabled: true,
                  ),
                ),
                const SizedBox(height: 10),
              ],
            ),
          const MyWalletsList(),
        ],
      ),
    );
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/smiths_provider.dart';
import 'package:provider/provider.dart';

class SmithCategory extends StatelessWidget {
  const SmithCategory({
    super.key,
    required this.categoryNumber,
  });

  final int categoryNumber;

  @override
  Widget build(BuildContext context) {
    final smithsP = Provider.of<Smiths>(context, listen: false);

    List<String> listCategory = [
      '${"online".tr()}: ${smithsP.nbrAutorities}',
      '${"offline".tr()}: ${smithsP.nbrMembers - smithsP.nbrAutorities}',
      '${"certified".tr()}:',
      '${"requested".tr()}:'
    ];

    return Container(
      decoration: const BoxDecoration(
          color: orangeC,
          borderRadius: BorderRadius.horizontal(right: Radius.circular(20))),
      height: 30,
      width: screenWidth / 5,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(listCategory[categoryNumber],
              style: const TextStyle(fontSize: 19, color: Colors.black)),
        ],
      ),
    );
  }
}

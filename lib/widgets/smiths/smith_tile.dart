import 'package:flutter/material.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/providers/polkadot_subscribe_sessions.dart';
import 'package:gecko_web/providers/smiths_provider.dart';
import 'package:provider/provider.dart';
import 'smith_details_widgets/smith_colors.dart';

class SmithTile extends StatelessWidget {
  const SmithTile({
    super.key,
    required this.smith,
  });

  final SmithData smith;

  @override
  Widget build(BuildContext context) {
    final polka = Provider.of<PolkadotProvider>(context, listen: false);
    final smithsP = Provider.of<Smiths>(context, listen: false);
    //ici c'est null ! :

    return Consumer2<PolkadotSubscribBlocksProvider,
            PolkadotSubscribSessionsProvider>(
        builder: (context, polkaSubBlock, polkaSubSession, _) {
      //log.d("${smith.username} ${smith.smithCertifBlockExpir}");
      return Stack(children: <Widget>[
        InkWell(
          onTap: () {
            smithsP.currentSmithDetails = smith;
            Scaffold.of(context).openEndDrawer();
          },
          child: Card(
            borderOnForeground: true,
            shape: RoundedRectangleBorder(
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              side: polka.listMyWallets.containsKey(smith.address)
                  ? const BorderSide(width: 5.0, color: Colors.blue)
                  : const BorderSide(width: 1.0, color: Colors.grey),
            ),
            color: tileColor(context, smith),
            elevation: 3,
            child: Row(
              children: [
                Expanded(
                  flex: 8,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      smith.name!,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20,
                        //fontWeight: FontWeight.bold,
                        decoration: TextDecoration.none,
                      ),
                    ),
                  ),
                ),
                if (smith.status == SmithStatus.outgoing ||
                    smith.status == SmithStatus.incoming)
                  Expanded(
                    flex: 2,
                    child: Column(children: [
                      if (smith.status == SmithStatus.outgoing)
                        const Icon(
                          Icons.arrow_downward,
                          size: 30,
                          color: Colors.red,
                        ),
                      if (smith.status == SmithStatus.incoming)
                        const Icon(
                          Icons.arrow_upward,
                          size: 30,
                          color: Colors.green,
                        ),
                    ]),
                  ),
              ],
            ),
          ),
        ),
      ]);
    });
  }
}

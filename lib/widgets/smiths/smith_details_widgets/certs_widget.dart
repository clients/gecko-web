import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/widgets/smiths/smith_details_widgets/smith_colors.dart';
import 'package:provider/provider.dart';

class CertsWidget extends StatelessWidget {
  const CertsWidget({
    required this.smith,
    required this.type,
    required this.getExpiration,
    required this.getCerts,
    required this.icon,
    super.key,
  });

  final String type;
  final Future<int> getExpiration;
  final Future<List<int>> getCerts;
  final String icon;
  final SmithData smith;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getCerts,
        builder: (BuildContext context, AsyncSnapshot<List<int>> certs) {
          if ( //certs.hasError ||
              // certs.data == null ||
              certs.connectionState != ConnectionState.done) {
            return const SizedBox();
          }

          return FutureBuilder(
              future: getExpiration,
              builder: (BuildContext context, AsyncSnapshot<int> expirations) {
                // if (expirations.hasError ||
                //     expirations.data == null ||
                //     expirations.connectionState != ConnectionState.done) {
                //   return const SizedBox();
                // }
                //return Text(expirations.data.toString());
                return Consumer<PolkadotSubscribBlocksProvider>(
                    builder: (context, polkaSubBlock, _) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.workspace_premium,
                            size: 40,
                            color: (smith.status == SmithStatus.online ||
                                    smith.status == SmithStatus.incoming ||
                                    smith.status == SmithStatus.outgoing ||
                                    smith.status == SmithStatus.offline)
                                ? (blockExpirationColor(
                                    smith,
                                    type,
                                    polkaSubBlock.blockNumber,
                                    expirations.data!))
                                : Colors.black,
                          ),
                          const SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${"sent".tr()}: ${certs.data?[1].toString() ?? '0'}",
                                style: mainStyle16,
                              ),
                              const SizedBox(width: 2),
                              //for issued certifications
                              Text(
                                "${"received".tr()}: ${certs.data?[0].toString() ?? '0'}",
                                style: TextStyle(
                                  fontSize: 16,
                                  decoration: TextDecoration.none,
                                  color: (smith.status == SmithStatus.online ||
                                          smith.status ==
                                              SmithStatus.incoming ||
                                          smith.status ==
                                              SmithStatus.outgoing ||
                                          smith.status == SmithStatus.offline)
                                      ? (blockExpirationColor(
                                          smith,
                                          type,
                                          polkaSubBlock.blockNumber,
                                          expirations.data!))
                                      : Colors.black,
                                ),
                              ),
                            ],
                          ),
                          // const SizedBox(width: 2),
                          // Image.asset('assets/arrow.png', height: 25),
                        ],
                      ),
                    ],
                  );
                });
              });
        });
  }
}

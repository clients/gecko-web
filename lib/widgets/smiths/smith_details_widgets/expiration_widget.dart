import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/widgets/smiths/smith_details_widgets/smith_colors.dart';
import 'package:provider/provider.dart';

class ExpirationWidget extends StatelessWidget {
  const ExpirationWidget({
    required this.type,
    required this.getExpiration,
    required this.smith,
    super.key,
  });

  final String type;
  final SmithData smith;
  final Future<int> getExpiration;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getExpiration,
        builder: (BuildContext context, AsyncSnapshot<int> expirations) {
          if (expirations.hasError ||
              expirations.data == null ||
              expirations.connectionState != ConnectionState.done) {
            return const SizedBox();
          }
          //return Text(expirations.data.toString());
          return Consumer<PolkadotSubscribBlocksProvider>(
            builder: (context, polkaSubBlock, _) {
              return Text(
                  "${"expires".tr()} ${DateFormat('d MMM yyyy').format(startBlockchainTime.add(Duration(seconds: expirations.data! * 6)))} BCT",
                  style: TextStyle(
                    fontSize: 14,
                    decoration: TextDecoration.none,
                    color: blockExpirationColor(smith, type,
                        polkaSubBlock.blockNumber, expirations.data!),
                  ));
            },
          );
        });
  }
}

import 'package:flutter/material.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/providers/polkadot_subscribe_sessions.dart';
import 'package:gecko_web/utils.dart';
import 'package:provider/provider.dart';

Color blockExpirationColor(
    SmithData smith, String type, int blockNumber, int blockExpiration) {
  if (blockNumber > (blockExpiration - daysToBlocks(5))) {
    //smith.tileColor = Colors.red;
    return Colors.red;
  }
  if (blockNumber > (blockExpiration - weekToBlocks(2))) {
    // if (smith.tileColor != Colors.red) {
    //   smith.tileColor = Colors.orange;
    // }
    return Colors.orange;
  }
  return Colors.black;
}

Color sessionExpirationColor(
    SmithData smith, int sessionNumber, int sessionExpiration) {
  if (sessionNumber > (sessionExpiration - daysToSessions(5))) {
    //smith.tileColor = Colors.red;
    return Colors.red;
  }
  if (sessionNumber > (sessionExpiration - weekToSessions(2))) {
    // if (smith.tileColor != Colors.red) {
    //   smith.tileColor = Colors.orange;
    // }
    return Colors.orange;
  }
  return Colors.black;
}

Color onlineColor(SmithStatus status) {
  if (status == SmithStatus.offline) {
    return Colors.red;
  }
  if (status == SmithStatus.online) {
    return Colors.green;
  }
  if (status == SmithStatus.outgoing || status == SmithStatus.incoming) {
    return Colors.orange;
  }
  return Colors.black;
}

Color tileColor(BuildContext context, SmithData smith) {
  final polkaSubBlock =
      Provider.of<PolkadotSubscribBlocksProvider>(context, listen: false);
  final polkaSubSession =
      Provider.of<PolkadotSubscribSessionsProvider>(context, listen: false);

  final blockNumber = polkaSubBlock.blockNumber;
  final blocExpirSmith = smith.smithCertifBlockExpir!;
  final blocExpirMember = smith.memberCertifBlockExpir!;
  final sessionNumber = polkaSubSession.sessionNumber;
  final sessionRotateKeys = smith.mustRotateKeys!;
  final sessionExpiration = smith.sessionKeyExpir!;

  // if (smith.username == 'Pini') {
  //   log.d('$sessionNumber > ($sessionExpiration - ${daysToSessions(5)})');
  //   log.d(sessionNumber > (sessionExpiration - daysToSessions(5)));
  // }
  if (smith.status == SmithStatus.online ||
      smith.status == SmithStatus.outgoing) {
    if (blockNumber > (blocExpirMember - daysToBlocks(5)) ||
        blockNumber > (blocExpirSmith - daysToBlocks(5)) ||
        sessionNumber > (sessionRotateKeys - daysToSessions(5))) {
      return Colors.red;
    } else if (blockNumber > (blocExpirMember - weekToBlocks(2)) ||
        blockNumber > (blocExpirSmith - weekToBlocks(2)) ||
        sessionNumber > (sessionRotateKeys - weekToSessions(2))) {
      return Colors.orange;
    } else {
      return Colors.black;
    }
  } else if (smith.status == SmithStatus.offline ||
      smith.status == SmithStatus.incoming) {
    if (blockNumber > (blocExpirMember - daysToBlocks(5)) ||
        blockNumber > (blocExpirSmith - daysToBlocks(5)) ||
        sessionNumber > (sessionExpiration - daysToSessions(5))) {
      return Colors.red;
    } else if (blockNumber > (blocExpirMember - weekToBlocks(2)) ||
        blockNumber > (blocExpirSmith - weekToBlocks(2)) ||
        sessionNumber > (sessionExpiration - weekToSessions(2))) {
      return Colors.orange;
    } else {
      return Colors.black;
    }
  } else {
    return Colors.black;
    // if (blockNumber > (blocExpirMember - daysToBlocks(5)) ||
    //     blockNumber > (blocExpirSmith - daysToBlocks(5))) {
    //   return Colors.red;
    // } else if (blockNumber > (blocExpirMember - weekToBlocks(2)) ||
    //     blockNumber > (blocExpirSmith - weekToBlocks(2))) {
    //   return Colors.orange;
    // } else {
    //   return Colors.black;
    // }
  }
}

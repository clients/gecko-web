import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/providers/polkadot_subscribe_sessions.dart';
import 'package:gecko_web/utils.dart';
import 'package:gecko_web/widgets/smiths/smith_details_widgets/smith_colors.dart';
import 'package:provider/provider.dart';

class SessionExpirationWidget extends StatelessWidget {
  const SessionExpirationWidget({
    required this.smith,
    super.key,
  });

  final SmithData smith;

  @override
  Widget build(BuildContext context) {
    DateTime dateSessionExpire =
        sessionNumberToDate(smith.sessionKeyExpir ?? 0);
    DateTime dateRotateKey = sessionNumberToDate(smith.mustRotateKeys ?? 0);
    return Consumer<PolkadotSubscribSessionsProvider>(
        builder: (context, polkaSubSession, _) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          smith.status == SmithStatus.offline
              ? polkaSubSession.sessionNumber < smith.sessionKeyExpir!
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.key,
                          weight: 700,
                          size: 40,
                          color: sessionExpirationColor(
                              smith,
                              polkaSubSession.sessionNumber,
                              smith.sessionKeyExpir!),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          children: [
                            Text(
                              "sessionExpire".tr(),
                              style: TextStyle(
                                  fontSize: 16,
                                  decoration: TextDecoration.none,
                                  color: sessionExpirationColor(
                                      smith,
                                      polkaSubSession.sessionNumber,
                                      smith.sessionKeyExpir!)),
                            ),
                            Text(
                              "${DateFormat('d MMM yyyy').format(dateSessionExpire)} (session: ${smith.sessionKeyExpir ?? 0})",
                              style: TextStyle(
                                  fontSize: 16,
                                  decoration: TextDecoration.none,
                                  color: sessionExpirationColor(
                                      smith,
                                      polkaSubSession.sessionNumber,
                                      smith.sessionKeyExpir!)),
                            ),
                          ],
                        ),
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(
                          Icons.key_off,
                          weight: 700,
                          size: 40,
                          color: Colors.red,
                        ),
                        const SizedBox(width: 10),
                        Text(
                          "keysexpired".tr(),
                          style: const TextStyle(
                              fontSize: 16,
                              decoration: TextDecoration.none,
                              color: Colors.red),
                        ),
                      ],
                    )
              : const Text(""),
          const SizedBox(width: 2),
          smith.status == SmithStatus.online
              ? polkaSubSession.sessionNumber < smith.mustRotateKeys!
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.key,
                          weight: 700,
                          size: 40,
                          color: sessionExpirationColor(
                              smith,
                              polkaSubSession.sessionNumber,
                              smith.mustRotateKeys!),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          children: [
                            Text(
                              "mustRotateKeys".tr(),
                              style: TextStyle(
                                fontSize: 16,
                                decoration: TextDecoration.none,
                                color: sessionExpirationColor(
                                    smith,
                                    polkaSubSession.sessionNumber,
                                    smith.mustRotateKeys!),
                              ),
                            ),
                            Text(
                              "${DateFormat('d MMM yyyy').format(dateRotateKey)} (session: ${smith.mustRotateKeys ?? 0})",
                              style: TextStyle(
                                fontSize: 16,
                                decoration: TextDecoration.none,
                                color: sessionExpirationColor(
                                    smith,
                                    polkaSubSession.sessionNumber,
                                    smith.mustRotateKeys!),
                              ),
                              //dateRotateKey > (sessionNumberToDate(polkaSubSession.sessionNumber))-(Duration(days: 30)) ? :
                            ),
                          ],
                        ),
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(
                          Icons.key_off,
                          weight: 700,
                          size: 40,
                          color: Colors.red,
                        ),
                        const SizedBox(width: 10),
                        Text(
                          "keysexpired".tr(),
                          style: const TextStyle(
                              fontSize: 16,
                              decoration: TextDecoration.none,
                              color: Colors.red),
                        ),
                      ],
                    )
              : const Text(""),
        ],
      );
    });
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/wallet_data.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/pay.dart';
import 'package:gecko_web/providers/my_theme.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/widgets/balance.dart';
import 'package:provider/provider.dart';

class PayPopup extends StatelessWidget {
  const PayPopup({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    final polka = Provider.of<PolkadotProvider>(context, listen: false);
    homeProvider.payAmount.text = '';

    return Consumer<PayProvider>(builder: (context, pay, _) {
      final theme = Provider.of<MyTheme>(context, listen: true);

      final canValidate = homeProvider.payAmount.text != '' &&
          polka.listMyWallets.isNotEmpty &&
          double.parse(homeProvider.payAmount.text) + 2 <=
              (polka.balanceCache[polka.selectedWallet.address] ?? 0) &&
          !(polka.balanceCache[homeProvider.currentAddress] == 0 &&
              double.parse(homeProvider.payAmount.text) < 5);

      return Container(
        height: 235,
        width: double.infinity,
        decoration: ShapeDecoration(
          color: theme.isDark
              ? const Color.fromARGB(255, 59, 55, 49)
              : const Color(0xffffeed1),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
        ),
        child: Padding(
          padding:
              const EdgeInsets.only(top: 10, bottom: 0, left: 24, right: 24),
          child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: [
                    Text(
                      'executeATransfer'.tr(),
                      style: TextStyle(
                          color: theme.isDark
                              ? Colors.grey[100]
                              : Colors.grey[900],
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(width: 10),
                    Consumer<PolkadotProvider>(builder: (context, _, __) {
                      return DropdownButton(
                        menuMaxHeight: 240,
                        value: polka.selectedWallet,
                        onChanged: (WalletData? newSelectedWallet) {
                          polka.selectedWallet = newSelectedWallet!;
                          pay.reload();
                        },
                        items:
                            polka.listMyWallets.values.map((WalletData wallet) {
                          return DropdownMenuItem(
                              value: wallet, child: Text(wallet.name));
                        }).toList(),
                      );
                    }),
                    const SizedBox(width: 10),
                    Balance(
                      address: polka.selectedWallet.address,
                      size: 14,
                      color:
                          theme.isDark ? Colors.grey[100]! : Colors.grey[900]!,
                    )
                  ],
                ),
                const SizedBox(height: 10),
                Text(
                  'amount'.tr(),
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color:
                          theme.isDark ? Colors.grey[300] : Colors.grey[600]),
                ),
                const SizedBox(height: 10),
                TextField(
                  controller: homeProvider.payAmount,
                  onEditingComplete:
                      canValidate ? () => toggleTransaction(context) : null,
                  autofocus: true,
                  maxLines: 1,
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                  onChanged: (_) => pay.reload(),
                  inputFormatters: <TextInputFormatter>[
                    // FilteringTextInputFormatter.digitsOnly,
                    FilteringTextInputFormatter.deny(',',
                        replacementString: '.'),
                    FilteringTextInputFormatter.allow(
                        RegExp(r'(^\d+\.?\d{0,2})')),
                  ],
                  // onChanged: (v) => _searchProvider.rebuildWidget(),
                  decoration: InputDecoration(
                    hintText: '0.00',
                    hintStyle: TextStyle(
                        color:
                            theme.isDark ? Colors.grey[100] : Colors.grey[600]),
                    suffix: Text(
                      currencyName,
                      style: TextStyle(
                          color: theme.isDark
                              ? Colors.grey[50]
                              : Colors.grey[600]),
                    ),
                    filled: true,
                    fillColor: Colors.transparent,
                    // border: OutlineInputBorder(
                    //     borderSide:
                    //         BorderSide(color: Colors.grey[500], width: 2),
                    //     borderRadius: BorderRadius.circular(8)),

                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: theme.isDark
                              ? Colors.grey[100]!
                              : Colors.grey[500]!,
                          width: 2),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: theme.isDark
                              ? Colors.grey[500]!
                              : Colors.grey[500]!,
                          width: 2),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    contentPadding: const EdgeInsets.all(15),
                  ),
                  style: TextStyle(
                    fontSize: 20,
                    color: theme.isDark ? Colors.grey[100] : Colors.black,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 20),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  SizedBox(
                    width: 200,
                    height: 40,
                    child: Consumer<HomeProvider>(builder: (context, homeP, _) {
                      return ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          foregroundColor: Colors.white,
                          disabledForegroundColor:
                              Colors.black.withOpacity(0.38),
                          disabledBackgroundColor:
                              Colors.black.withOpacity(0.12),
                          backgroundColor: orangeC,
                          elevation: 4,
                        ),
                        onPressed: canValidate
                            ? () => toggleTransaction(context)
                            : null,
                        child: Text(
                          'executeTheTransfer'.tr(),
                          style: const TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w500),
                        ),
                      );
                    }),
                  ),
                ]),
                // IconButton(
                //   iconSize: 40,
                //   icon: const Icon(Icons.cancel_outlined),
                //   onPressed: () {
                //     Navigator.pop(context);
                //   },
                // ),
                const SizedBox(height: 20),
              ]),
        ),
      );
    });
  }

  toggleTransaction(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    final polka = Provider.of<PolkadotProvider>(context, listen: false);
    polka.payExtension(polka.selectedWallet.address,
        homeProvider.currentAddress, double.parse(homeProvider.payAmount.text));
  }
}

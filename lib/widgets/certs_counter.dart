import 'package:flutter/material.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:provider/provider.dart';

class CertsCounter extends StatelessWidget {
  const CertsCounter(
      {super.key,
      required this.address,
      required this.isSent,
      required this.isSmith});
  final String address;
  final bool isSent;
  final bool isSmith;

  @override
  Widget build(BuildContext context) {
    return Consumer2<PolkadotProvider, PolkadotSubscribBlocksProvider>(
        builder: (context, polka, polkaBlock, _) {
      // log.d(polka.certsSmithCounterCache[address]![isSent ? 1 : 0]);
      return Text(
          isSmith
              ? '(${polka.certsSmithCounterCache[address]![isSent ? 1 : 0]})'
              : '(${polka.certsCounterCache[address]![isSent ? 1 : 0]})',
          style: const TextStyle(color: Colors.black));
    });
  }
}

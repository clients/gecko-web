import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/indexer.dart';
import 'package:gecko_web/queries.dart';
import 'package:gecko_web/widgets/cert_tile.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class CertsList extends StatelessWidget {
  const CertsList({super.key, required this.address, required this.isSend, required this.isSmith});
  final String address;
  final bool isSend;
  final bool isSmith;

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final appBarHeight = AppBar().preferredSize.height;
    final windowHeight = screenHeight - appBarHeight - 140;

    return GraphQLProvider(
      client: ValueNotifier(Indexer().client),
      child: Query(
        options: QueryOptions(
          document: gql(certRequestSelector(isSend, isSmith)),
          variables: <String, dynamic>{
            'address': address,
          },
        ),
        builder: (QueryResult result, {fetchMore, refetch}) {
          if (result.isLoading && result.data == null) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          if (result.hasException || result.data == null) {
            log.e('Error Indexer: ${result.exception}');
            return Column(children: <Widget>[
              const SizedBox(height: 50),
              Text(
                "noNetworkNoHistory".tr(),
                textAlign: TextAlign.center,
                style: const TextStyle(fontSize: 18),
              )
            ]);
          } else if (result.data?[isSmith ? 'smith_cert' : 'cert']?.isEmpty) {
            return Column(children: <Widget>[
              const SizedBox(height: 50),
              Text(
                "noDataToDisplay".tr(),
                style: const TextStyle(fontSize: 18),
              )
            ]);
          }
          // Build history list
          return SizedBox(
            height: windowHeight,
            child: ListView(
              children: <Widget>[certsView(result, isSend, isSmith)],
            ),
          );
        },
      ),
    );
  }

  Widget certsView(QueryResult result, bool isSend, bool isSmith) {
    List listCerts = [];
    DateTime date = DateTime.parse("19700101");
    String dateForm = '';
    String issuerAddress = '';
    String issuerName = '';
    final List certsData = isSmith ? result.data!['smith_cert'] : result.data!['cert'];

    for (final cert in certsData) {
      if (isSmith) {
        issuerAddress = cert[isSend ? 'receiver' : 'issuer']['identity']['accountId'] ?? '';
        issuerName = cert[isSend ? 'receiver' : 'issuer']['identity']['name'] ?? '';
        date = DateTime.parse(cert[isSend ? 'receiver' : 'issuer']['identity']['createdIn']['block']['timestamp']  ?? '19700101');
      } else {
        issuerAddress = cert[isSend ? 'receiver' : 'issuer']['accountId'] ?? '';
        issuerName = cert[isSend ? 'receiver' : 'issuer']['name'] ?? '';

        date = DateTime.parse(cert[isSend ? 'receiver' : 'issuer']['createdIn']['block']['timestamp'] ?? '19700101');
      }

      final dp = DateTime(date.year, date.month, date.day);
      dateForm = '${dp.day}-${dp.month}-${dp.year}';

      listCerts.add({'address': issuerAddress, 'name': issuerName, 'date': dateForm});
    }

    return result.data == null
        ? Column(children: <Widget>[
            const SizedBox(height: 50),
            Text(
              "noTransactionToDisplay".tr(),
              style: const TextStyle(fontSize: 18),
            )
          ])
        : Column(children: <Widget>[
            CertTile(listCerts: listCerts),
          ]);
  }
}

String certRequestSelector(bool isSend, bool isSmith) {
  if (isSmith) {
    if (isSend) {
      return getSmithCertsSent;
    } else {
      return getSmithCertsReceived;
    }
  } else {
    if (isSend) {
      return getCertsSent;
    } else {
      return getCertsReceived;
    }
  }
}

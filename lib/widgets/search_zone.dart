import 'dart:async';
import 'package:flutter/material.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/search.dart';
import 'package:gecko_web/widgets/loading.dart';
import 'package:provider/provider.dart';

class SearchZone extends StatelessWidget {
  const SearchZone({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final searchProvider = Provider.of<SearchProvider>(context, listen: false);

    HomeProvider homeProvider =
        Provider.of<HomeProvider>(context, listen: false);
    final debouncer = Debouncer(milliseconds: 300);

    return SizedBox(
      width: 600,
      child: TextField(
        focusNode: searchProvider.searchFocus,
        cursorColor: Colors.grey[600],
        controller: searchProvider.searchController,
        autofocus: true,
        maxLines: 1,
        textAlign: TextAlign.left,
        onChanged: (v) {
          debouncer.run(() => searchProvider.reload());
        },
        onSubmitted: (value) {
          searchProvider.reload();
        },
        decoration: InputDecoration(
          filled: true,
          fillColor: Theme.of(context).primaryColor,
          prefixIconConstraints: const BoxConstraints(
            minHeight: 20,
          ),
          prefixIcon: const Padding(
            padding: EdgeInsets.symmetric(horizontal: 17),
            child:
                Image(image: AssetImage('assets/loupe-noire.png'), height: 30),
          ),
          suffixIcon: Consumer<HomeProvider>(builder: (context, homeP, _) {
            return Consumer<SearchProvider>(builder: (context, search, _) {
              if (search.isLoading) {
                return Visibility(
                    visible: search.isLoading,
                    child: const Loading(size: 8, stroke: 3));
              } else {
                return searchProvider.searchController.text.isNotEmpty ||
                        homeProvider.currentAddress != ''
                    ? IconButton(
                        icon: Icon(
                          Icons.close,
                          color: Colors.grey[600],
                        ),
                        onPressed: () {
                          homeProvider.closeProfile();
                        },
                      )
                    : const Text('');
              }
            });
          }),
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[500]!, width: 1),
              borderRadius: BorderRadius.circular(8)),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[500]!, width: 2),
            borderRadius: BorderRadius.circular(8),
          ),
          contentPadding: const EdgeInsets.all(10),
        ),
        style: const TextStyle(
          fontSize: 18,
          color: Colors.black,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }
}

class Debouncer {
  final int milliseconds;
  Timer? _timer;

  Debouncer({required this.milliseconds});

  run(VoidCallback action) {
    _timer?.cancel();
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}

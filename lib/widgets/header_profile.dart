import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/indexer.dart';
import 'package:gecko_web/queries.dart';
import 'package:gecko_web/utils.dart';
import 'package:gecko_web/widgets/balance.dart';
import 'package:gecko_web/widgets/get_certs.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';

class HeaderProfileView extends StatelessWidget {
  const HeaderProfileView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: true);

    return Stack(children: <Widget>[
      Container(
          height: 210,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                yellowC,
                Color(0xFFE7811A),
              ],
            ),
          )),
      Material(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(4),
          child: IconButton(
              hoverColor: Colors.black.withOpacity(0.2),
              onPressed: () {
                homeProvider.backProfile();
              },
              icon: const Icon(Icons.arrow_back, color: Colors.black),
              iconSize: 25),
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(left: 30, right: 40),
        child: Row(children: <Widget>[
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            const SizedBox(height: 40),
            Row(children: [
              InkWell(
                key: const Key('copyPubkey'),
                onTap: () {
                  Clipboard.setData(ClipboardData(text: homeProvider.currentAddress));
                  homeProvider.snackCopyKey(context, homeProvider.currentAddress);
                },
                child: Text(
                  getShortPubkey(homeProvider.currentAddress),
                  style: TextStyle(
                    color: Colors.grey[900],
                    fontSize: 22,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              Material(
                color: Colors.transparent,
                child: Padding(
                  padding: const EdgeInsets.all(4),
                  child: IconButton(
                      hoverColor: Colors.black.withOpacity(0.2),
                      tooltip: "Copier l'adresse",
                      onPressed: () {
                        Clipboard.setData(ClipboardData(text: homeProvider.currentAddress));
                        homeProvider.snackCopyKey(context, homeProvider.currentAddress);
                      },
                      icon: const Icon(Icons.copy, color: Colors.black),
                      iconSize: 25),
                ),
              ),
            ]),
            const SizedBox(height: 15),
            Balance(address: homeProvider.currentAddress, size: 20),
            const SizedBox(height: 10),
            InkWell(
              onTap: () {
                homeProvider.showCertsView = true;
                homeProvider.reload();
                // if (screenWidth < screenBreakpoint) {
                //   Scaffold.of(context).openEndDrawer();
                // }
                // context.router.push(CertificationsRoute(
                //     address: homeProvider.currentAddress,
                //     username: homeProvider.currentName));
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (homeProvider.currentName == '')
                    GraphQLProvider(
                      client: ValueNotifier(Indexer().client),
                      child: Query(
                          options: QueryOptions(
                            document: gql(getNameByAddressQ),
                            variables: {
                              'name': homeProvider.currentAddress,
                            },
                          ),
                          builder: (QueryResult result, {VoidCallback? refetch, FetchMore? fetchMore}) {
                            if (result.hasException) {
                              return Text(result.exception.toString());
                            }

                            homeProvider.currentName = result.data?['accountByPk']?['identity']?['name'] ?? '';

                            return Text(homeProvider.currentName, style: TextStyle(color: Colors.grey[900], fontSize: 18, fontWeight: FontWeight.w600));
                          }),
                    )
                  else
                    Text(homeProvider.currentName, style: TextStyle(color: Colors.grey[900], fontSize: 18, fontWeight: FontWeight.w600)),
                  GetCerts(address: homeProvider.currentAddress, size: 17, color: Colors.grey[900]!),
                ],
              ),
            ),
            const SizedBox(height: 40),
          ]),
          const Spacer(),
          Column(children: <Widget>[
            QrImageWidget(
              data: homeProvider.currentAddress,
              size: 150,
            ),
          ]),
        ]),
      ),
    ]);
  }
}

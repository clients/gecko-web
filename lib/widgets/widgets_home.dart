import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:gecko_web/widgets/list_latest_identities.dart';
import 'package:gecko_web/widgets/list_latest_transactions.dart';
import 'package:provider/provider.dart';

class InfosView extends StatefulWidget {
  const InfosView({
    super.key,
  });

  @override
  InfosViewState createState() => InfosViewState();
}

class InfosViewState extends State<InfosView> {
  @override
  Widget build(BuildContext context) {
    final homeP = Provider.of<HomeProvider>(homeContext, listen: false);
    return SizedBox(
      height: screenHeight,
      child: Consumer<PolkadotProvider>(builder: (context, polka, _) {
        final listDuniterEndpointsWithLocal = listDuniterEndpoints!.toList();
        listDuniterEndpointsWithLocal.add('ws://127.0.0.1:9944/ws');
        return Column(
          children: [
            const SizedBox(height: 10),
            Row(children: [
              const SizedBox(width: 10),
              polka.duniterNodeStatus,
              const SizedBox(width: 5),
              const Text(
                'Duniter:',
                style: TextStyle(fontWeight: FontWeight.w100, fontSize: 12),
              ),
              const SizedBox(width: 10),
              DropdownButton<String>(
                value: currentDuniterEndpoint,
                style: TextStyle(fontWeight: FontWeight.w100, fontSize: 12, color: Theme.of(context).textTheme.bodyLarge!.color),
                items: listDuniterEndpointsWithLocal.map((var value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (String? newValue) {
                  setState(() {
                    polka.connectNode([newValue!]);
                  });
                },
              ),
              // const SizedBox(width: 20),
              const Spacer(),
              Consumer<PolkadotSubscribBlocksProvider>(builder: (context, polkaBlock, _) {
                return Text(
                  polkaBlock.blockNumber == 0 ? '' : 'block: ${polkaBlock.blockNumber}',
                  style: TextStyle(fontSize: 12, color: Theme.of(context).textTheme.bodyLarge!.color),
                );
              }),
              const Spacer(),
            ]),
            Row(
              children: [
                const SizedBox(width: 10),
                homeP.indexerEndpointStatus,
                const SizedBox(width: 5),
                const Text(
                  'Indexer:',
                  style: TextStyle(fontWeight: FontWeight.w100, fontSize: 12),
                ),
                const SizedBox(width: 10),
                DropdownButton<String>(
                  value: currentIndexerEndpoint,
                  style: TextStyle(fontWeight: FontWeight.w100, fontSize: 12, color: Theme.of(context).textTheme.bodyLarge!.color),
                  items: listIndexerEndpoints!.map((var value2) {
                    return DropdownMenuItem<String>(
                      value: value2,
                      child: Text(value2),
                    );
                  }).toList(),
                  onChanged: (String? newValue2) {
                    setState(() {
                      homeP.getValidIndexerEndpoint(newValue2!);
                    });
                  },
                ),
              ],
            ),
            const Spacer(),
            const LatestIdentityWidget(),
            const SizedBox(height: 20),
            const LatestTransactionsWidget(),
            const SizedBox(height: 15),
          ],
        );
      }),
    );
  }
}

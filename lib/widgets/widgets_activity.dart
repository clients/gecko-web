import 'package:easy_localization/easy_localization.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/indexer.dart';
import 'package:gecko_web/queries.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/utils.dart';
import 'package:gecko_web/widgets/cert_tile.dart';
import 'package:gecko_web/widgets/loading.dart';
import 'package:gecko_web/widgets/transaction_in_progress.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

final ScrollController scrollController = ScrollController();

class HistoryQuery extends StatelessWidget {
  const HistoryQuery({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: true);
    FetchMoreOptions? opts;

    return Expanded(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        GraphQLProvider(
          client: ValueNotifier(Indexer.relay.client),
          child: Query(
            options: QueryOptions(
              document: gql(getHistoryByAddressQ),
              variables: <String, dynamic>{'address': homeProvider.currentAddress, 'number': 20, 'cursor': null},
            ),
            builder: (QueryResult result, {fetchMore, refetch}) {
              if (result.isLoading && result.data == null) {
                return const Center(
                    child: Loading(
                  size: 30,
                ));
              }

              if (result.hasException) {
                log.d('Error Indexer: ${result.exception}');
                return Column(children: <Widget>[
                  const SizedBox(height: 50),
                  Text(
                    "noNetworkNoHistory".tr(),
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 18),
                  )
                ]);
              } else if (result.data?['transferConnection']?['edges'].isEmpty) {
                return Column(children: <Widget>[
                  const SizedBox(height: 50),
                  TransactionInProgress(address: homeProvider.currentAddress),
                  Text(
                    "noDataToDisplay".tr(),
                    style: const TextStyle(fontSize: 18),
                  )
                ]);
              }

              if (result.isNotLoading) {
                opts = homeProvider.mergeQueryResult(context, 'transferConnection', result, address: homeProvider.currentAddress);
              }

              final Map pageInfo = result.data!['transferConnection']['pageInfo'];

              // Build history list
              return NotificationListener(
                  child: Builder(
                    builder: (context) => Expanded(
                      child: ListView(
                        key: const Key('listTransactions'),
                        controller: scrollController,
                        children: <Widget>[TransactionInProgress(address: homeProvider.currentAddress), _historyView(context, result, pageInfo)],
                      ),
                    ),
                  ),
                  onNotification: (dynamic t) {
                    if (t is ScrollEndNotification &&
                        scrollController.position.pixels >= scrollController.position.maxScrollExtent * 0.7 &&
                        pageInfo['hasNextPage'] &&
                        result.isNotLoading) {
                      fetchMore!(opts!);
                    }
                    return true;
                  });
            },
          ),
        ),
      ],
    ));
  }
}

Widget _historyView(context, result, pageInfo) {
  HomeProvider homeProvider = Provider.of<HomeProvider>(context, listen: false);

  return (homeProvider.parsedHistoryData[homeProvider.currentAddress] ?? []).isEmpty
      ? Column(children: <Widget>[
          const SizedBox(height: 50),
          Text(
            "noTransactionToDisplay".tr(),
            style: const TextStyle(fontSize: 18),
          )
        ])
      : Column(children: <Widget>[
          _getTransactionTile(context, homeProvider),
          if (result.isLoading)
            const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Loading(),
              ],
            ),
          if (!pageInfo!['hasNextPage'])
            const Column(
              children: <Widget>[
                SizedBox(height: 15),
                Text("Début de l'historique.", textAlign: TextAlign.center, style: TextStyle(fontSize: 20)),
                SizedBox(height: 15)
              ],
            )
        ]);
}

Widget _getTransactionTile(BuildContext context, HomeProvider homeProvider) {
  int keyID = 0;
  String? dateDelimiter;
  String? lastDateDelimiter;

  bool isTody = false;
  bool isYesterday = false;
  bool isThisWeek = false;
  bool isMigrationTime = false;
  bool isMigrationPassed = false;

  // final startBlockchainTimeHuman =
  //     "${startBlockchainTime.day} ${monthsInYear[startBlockchainTime.month]!.substring(0, 4)}. ${startBlockchainTime.year} - ${startBlockchainTime.hour}:${startBlockchainTime.minute}";

  final startBlockchainTimeHuman =
      "${DateFormat.yMMMMEEEEd().format(startBlockchainTime.toLocal())} - ${DateFormat.Hm().format(startBlockchainTime.toLocal())}";

  return Column(
      children: homeProvider.parsedHistoryData[homeProvider.currentAddress]!.map((repository) {
    final now = DateTime.now();
    final DateTime date = repository[0].toLocal();

    late String dateForm;
    if ({4, 10, 11, 12}.contains(date.month)) {
      dateForm = "${date.day} ${monthsInYear[date.month]!.substring(0, 3)}.";
    } else if ({1, 2, 7, 9}.contains(date.month)) {
      dateForm = "${date.day} ${monthsInYear[date.month]!.substring(0, 4)}.";
    } else {
      dateForm = "${date.day} ${monthsInYear[date.month]}";
    }

    int weekNumber(DateTime date) {
      int dayOfYear = int.parse(DateFormat("D").format(date));
      return ((dayOfYear - date.weekday + 10) / 7).floor();
    }

    final transactionDate = DateTime(date.year, date.month, date.day);
    final todayDate = DateTime(now.year, now.month, now.day);
    final yesterdayDate = DateTime(now.year, now.month, now.day - 1);

    if (transactionDate == todayDate && !isTody) {
      dateDelimiter = lastDateDelimiter = "today".tr();
      isTody = true;
    } else if (transactionDate == yesterdayDate && !isYesterday) {
      dateDelimiter = lastDateDelimiter = "yesterday".tr();
      isYesterday = true;
    } else if (weekNumber(date) == weekNumber(now) &&
        date.year == now.year &&
        lastDateDelimiter != "thisWeek".tr() &&
        transactionDate != yesterdayDate &&
        transactionDate != todayDate &&
        !isThisWeek) {
      dateDelimiter = lastDateDelimiter = "thisWeek".tr();
      isThisWeek = true;
    } else if (lastDateDelimiter != monthsInYear[date.month] &&
        lastDateDelimiter != "${monthsInYear[date.month]} ${date.year}" &&
        transactionDate != todayDate &&
        transactionDate != yesterdayDate &&
        !(weekNumber(date) == weekNumber(now) && date.year == now.year)) {
      if (date.year == now.year) {
        dateDelimiter = lastDateDelimiter = monthsInYear[date.month];
      } else {
        dateDelimiter = lastDateDelimiter = "${monthsInYear[date.month]} ${date.year}";
      }
    } else {
      dateDelimiter = null;
    }

    if (isMigrationTime) {
      isMigrationPassed = true;
    }

    if (date.compareTo(startBlockchainTime) < 0) {
      isMigrationTime = true;
    } else {
      isMigrationTime = false;
    }

    return Column(children: <Widget>[
      if (isMigrationTime && !isMigrationPassed)
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const Image(image: AssetImage('assets/party.png'), height: 35),
              Column(
                children: [
                  Text(
                    'blockchainStart'.tr(),
                    style: const TextStyle(fontSize: 23, color: Colors.blueAccent, fontWeight: FontWeight.w500),
                  ),
                  const SizedBox(height: 1),
                  Text(
                    startBlockchainTimeHuman,
                    style: const TextStyle(fontSize: 16, color: Colors.blueAccent, fontWeight: FontWeight.w500),
                  ),
                ],
              ),
              const Image(image: AssetImage('assets/party.png'), height: 35),
            ],
          ),
        ),
      if (dateDelimiter != null)
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Text(
            dateDelimiter!,
            style: const TextStyle(fontSize: 18, color: orangeC, fontWeight: FontWeight.w300),
          ),
        ),
      Padding(
        padding: const EdgeInsets.only(right: 0),
        child: ListTile(
            key: Key('transaction${keyID++}'),
            contentPadding: const EdgeInsets.only(left: 20, right: 30, top: 15, bottom: 15),
            leading: ClipOval(
              child: defaultAvatar(50),
            ),
            title: Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: Text(
                getShortPubkey(repository[1]),
                style: GoogleFonts.robotoMono(
                  fontSize: 16,
                ),
              ),
            ),
            subtitle: RichText(
              text: TextSpan(
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.grey[700],
                ),
                children: <TextSpan>[
                  TextSpan(text: dateForm, style: TextStyle(color: Theme.of(context).textTheme.titleLarge!.color)),
                  if (repository[2] != '')
                    TextSpan(
                      text: '  ·  ',
                      style: TextStyle(
                        fontSize: 20,
                        color: Theme.of(context).textTheme.titleLarge!.color,
                      ),
                    ),
                  TextSpan(
                    text: repository[2],
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      color: Theme.of(context).textTheme.titleLarge!.color,
                    ),
                  ),
                ],
              ),
            ),
            trailing: Text("${repository[3]} $currencyName",
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: repository[4] == 'RECEIVED' ? Colors.green : Colors.blue),
                textAlign: TextAlign.justify),
            dense: false,
            isThreeLine: false,
            onTap: () {
              homeProvider.currentAddress = repository[1];
              homeProvider.currentName = repository[2];
              homeProvider.reload();
            }),
      ),
    ]);
  }).toList());
}

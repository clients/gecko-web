import 'package:flutter/material.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/polkadot_subscribe_blocks.dart';
import 'package:provider/provider.dart';

Map<String, List<int>> certsCache = {};

class GetCerts extends StatelessWidget {
  const GetCerts({
    super.key,
    required this.address,
    required this.size,
    required this.color,
  });

  final String address;
  final double size;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final polka = Provider.of<PolkadotProvider>(context, listen: false);

    return Column(children: <Widget>[
      Consumer<PolkadotSubscribBlocksProvider>(
          builder: (context, polkaBlock, _) {
        return FutureBuilder(
            future: polka.getCerts(address),
            builder: (BuildContext context, AsyncSnapshot<List<int>> certs) {
              if (certs.hasError) {
                return const SizedBox();
              }
              if (certs.connectionState != ConnectionState.done) {
                if (certsCache[address] != null &&
                    (certsCache[address]![0] != 0 ||
                        certsCache[address]![1] != 0)) {
                  if (certs.data == null) {
                    return SizedBox(height: size);
                  }

                  Row(
                    children: [
                      Image.asset('assets/medal.png', height: size),
                      const SizedBox(width: 1),
                      Text(certsCache[address]?[0].toString() ?? '0',
                          style: TextStyle(fontSize: size, color: color)),
                      const SizedBox(width: 2),
                      Text(
                        "(${certsCache[address]?[1].toString() ?? '0'})",
                        style: TextStyle(fontSize: size * 0.6, color: color),
                      )
                    ],
                  );
                } else {
                  return SizedBox(height: size - 1);
                }
              }

              certsCache[address] = certs.data ?? [];

              return certsCache[address] != null &&
                      (certsCache[address]![0] != 0 ||
                          certsCache[address]![1] != 0)
                  ? Row(
                      children: [
                        Image.asset('assets/medal.png', height: size),
                        const SizedBox(width: 1),
                        Text(certsCache[address]?[0].toString() ?? '0',
                            style: TextStyle(fontSize: size, color: color)),
                        const SizedBox(width: 2),
                        Text(
                          "(${certsCache[address]?[1].toString() ?? '0'})",
                          style: TextStyle(fontSize: size * 0.6, color: color),
                        )
                      ],
                    )
                  : const Text('');
            });
      }),
    ]);
  }
}

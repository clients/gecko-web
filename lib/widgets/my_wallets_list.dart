// ignore_for_file: use_build_context_synchronously

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/wallet_data.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/indexer.dart';
import 'package:gecko_web/providers/my_theme.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/utils.dart';
import 'package:gecko_web/widgets/balance.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class MyWalletsList extends StatelessWidget {
  const MyWalletsList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    final myTheme = Provider.of<MyTheme>(context, listen: false);
    late Uri downloadLink;

    switch (homeProvider.browserName) {
      case 'chrome':
        downloadLink = Uri.parse('https://chromewebstore.google.com/detail/duniter-connect/ecgekdejmghmkookmoafihgofnnlkleb?hl=fr');
        break;
      case 'firefox':
        downloadLink = Uri.parse('https://addons.mozilla.org/fr/firefox/addon/duniter-connect/');
        break;
      default:
        downloadLink = Uri.parse('https://addons.mozilla.org/fr/firefox/addon/duniter-connect/');
        break;
    }

    return Consumer<PolkadotProvider>(builder: (context, polka, _) {
      return Container(
        color: Theme.of(context).primaryColorDark,
        constraints: const BoxConstraints(maxHeight: 400),
        width: 280,
        child: SingleChildScrollView(
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            if (polka.haveExtension == null)
              const Padding(
                padding: EdgeInsets.all(8),
                child: Text("Chargement de l'extension"),
              )
            else if (!polka.haveExtension!)
              InkWell(
                onTap: () => launchUrl(downloadLink),
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // Material(
                      //   color: Colors.transparent,
                      //   child: SizedBox(
                      //     height: 28,
                      //     width: 28,
                      //     child: IconButton(
                      //       padding: EdgeInsets.zero,
                      //       onPressed: () => polka.getInjectedAccounts(),
                      //       icon: const Icon(Icons.replay_outlined),
                      //       iconSize: 17,
                      //       // splashRadius: 32,
                      //       tooltip: "Vérifier l'extension",
                      //     ),
                      //   ),
                      // ),
                      // const SizedBox(width: 10),
                      Text(
                        "Cliquez ici pour installer l'extension\n${homeProvider.browserName} Duniter-connect",
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.w100),
                      ),
                    ],
                  ),
                ),
              )
            else if (polka.listMyWallets.isEmpty)
              const Padding(
                padding: EdgeInsets.all(8),
                child: Text('Aucun portefeuille'),
              )
            else
              Column(
                children: [
                  const SizedBox(height: 10),
                  Text(
                    'myWallets'.tr(),
                    style: const TextStyle(fontSize: 16),
                  ),
                  const SizedBox(height: 10),
                  for (var wallet in polka.listMyWallets.values)
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 0),
                      child: Material(
                        color: Theme.of(context).primaryColorDark,
                        child: ListTile(
                            // horizontalTitleGap: 40,
                            leading: wallet.status == IdentityStatus.member
                                ? Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/medal.png',
                                        height: 17,
                                        color: Colors.blueAccent,
                                      ),
                                    ],
                                  )
                                : null,
                            minLeadingWidth: 0,
                            horizontalTitleGap: 7,
                            contentPadding: EdgeInsets.only(left: wallet.status == IdentityStatus.none ? 15 : 5, right: 15, top: 3, bottom: 3),
                            hoverColor: Colors.black.withOpacity(myTheme.isDark ? 0.9 : 0.1),
                            title: Row(children: <Widget>[
                              Text(getShortPubkey(wallet.address),
                                  style: TextStyle(color: Theme.of(context).textTheme.bodyMedium!.color, fontSize: 15, fontWeight: FontWeight.w500),
                                  textAlign: TextAlign.center),
                            ]),
                            trailing: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [Balance(address: wallet.address, size: 14, color: Theme.of(context).textTheme.titleLarge!.color!)]),
                            subtitle: Row(children: <Widget>[
                              Text(wallet.name,
                                  style: TextStyle(
                                      color: Theme.of(context).textTheme.titleLarge!.color,
                                      fontSize: 14,
                                      // fontFamily: 'Monospace',
                                      fontWeight: FontWeight.w500),
                                  textAlign: TextAlign.center),
                            ]),
                            dense: false,
                            isThreeLine: false,
                            onTap: () async {
                              homeProvider.currentAddress = wallet.address;
                              homeProvider.currentName = (await Indexer().namesByAddress([wallet.address]))[wallet.address] ?? '';
                              homeProvider.reload();
                              if (screenWidth < screenBreakpoint) {
                                Scaffold.of(context).openEndDrawer();
                              }
                            }),
                      ),
                    ),
                ],
              )
          ]),
        ),
      );
    });
  }
}

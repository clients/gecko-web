import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';

class Loading extends StatelessWidget {
  const Loading({
    this.size = 15,
    this.stroke = 2,
    super.key,
  });

  final double size;
  final double stroke;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: size,
      width: size,
      child: CircularProgressIndicator(
        color: orangeC,
        strokeWidth: stroke,
      ),
    );
  }
}

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/models/smiths_data.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/providers/smiths_provider.dart';
import 'package:gecko_web/screens/certifications.dart';

import 'package:gecko_web/widgets/smiths/fixed_column_widgets/fixed_column.dart';

import 'package:gecko_web/widgets/smiths/grid_widget.dart';
import 'package:gecko_web/widgets/smiths/loading.dart';
import 'package:gecko_web/widgets/smiths/smith_category.dart';
import 'package:gecko_web/widgets/smiths/smith_details_widgets/smith_details.dart';
import 'package:gecko_web/widgets/smiths/smith_tile.dart';
import 'package:provider/provider.dart';
import 'package:gecko_web/providers/my_theme.dart';

@RoutePage()
class SmithsPage extends StatelessWidget {
  const SmithsPage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final smithsP = Provider.of<Smiths>(context, listen: false);
    final polka = Provider.of<PolkadotProvider>(context, listen: false);
    if (!polka.nodeConnected) {
      homeContext = context;
    }

    // return FutureBuilder(
    //   future: homeProvider.getBrowserInfo(),
    //   builder: (context, browserInfo) {
    //     if (browserInfo.connectionState != ConnectionState.done) {
    //       return const SizedBox();
    //     }
    return Consumer<MyTheme>(builder: (context, myTheme, _) {
      return MaterialApp(
        theme: myTheme.getTheme(),
        home: Scaffold(
          appBar: AppBar(
            //actions: const [SizedBox()],
            leading: Row(
              children: [
                IconButton(
                    icon: const Icon(
                      Icons.arrow_back,
                      //color: Theme.of(context).primaryColorLight,
                    ),
                    onPressed: () {
                      context.router.canPop()
                          ? context.router.maybePop()
                          : context.router.pushNamed('/');
                    }),
              ],
            ),
            title: Text(
              'smithTitle'.tr(),
              //style: TextStyle(color: Theme.of(context).primaryColorLight),
            ),
            //backgroundColor: Theme.of(context).primaryColorDark,
          ),
          endDrawer: Consumer<Smiths>(builder: (context, smithsP, _) {
            return Drawer(
                width: 500,
                child: smithsP.showCertsView
                    ? CertificationsScreen(
                        address: smithsP.currentSmithDetails.address,
                        username: smithsP.currentSmithDetails.name!,
                        isSmith: smithsP.isSmithCerts)
                    : SmithDetails(
                        key: key,
                      ));
          }),
          body: FutureBuilder(
            // Here we initialize every async function we need before start the app
            future: smithsP.asyncStartup(context),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }
              if (snapshot.connectionState != ConnectionState.done &&
                  !smithsP.isSmithStartupLoaded) {
                return LoadingWidget(loadingText: 'loading'.tr());
              }

              screenWidth = MediaQuery.of(context).size.width;
              screenHeight = MediaQuery.of(context).size.width;

              // fill a list en SmithTiles widgets
              final List<SmithTile> smithsTilesOnline = [];
              final List<SmithTile> smithsTilesOffline = [];
              final List<SmithTile> smithsTilesRequested = [];
              final List<SmithTile> smithsTilesCertified = [];

//               log.i('''
// currentSession: ${smithsP.currentSession}
// smithsP.listSmiths.values.first.address: ${smithsP.listSmiths.values.first.address}
// ''');
              for (final smith in smithsP.listSmiths) {
                if (smith.status == SmithStatus.online ||
                    smith.status == SmithStatus.outgoing) {
                  smithsTilesOnline.add(
                    SmithTile(smith: smith),
                  );
                } else if (smith.status == SmithStatus.offline ||
                    smith.status == SmithStatus.incoming) {
                  smithsTilesOffline.add(
                    SmithTile(smith: smith),
                  );
                } else if (smith.status == SmithStatus.requested) {
                  smithsTilesRequested.add(
                    SmithTile(smith: smith),
                  );
                } else if (smith.status == SmithStatus.certified) {
                  smithsTilesCertified.add(
                    SmithTile(smith: smith),
                  );
                }
              }
              int nTule;
              if (screenWidth >= 1500) {
                nTule = 6;
              } else if (screenWidth >= 1200) {
                nTule = 4;
              } else {
                nTule = 3;
              }

              // Build the list of SmithTiles
              return Stack(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            opacity: 0.3,
                            image: AssetImage("assets/smith_background.jpg"),
                            fit: BoxFit.fill)),
                  ),
                  SingleChildScrollView(
                    padding: EdgeInsets.fromLTRB(0, 10.0, screenWidth / 3, 0),
                    child: Consumer<Smiths>(builder: (context, smithsP, _) {
                      return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            // Counters(
                            //   nbrAutorities: smithsP.nbrAutorities,
                            //   nbrMembers: smithsP.nbrMembers,
                            //   nbrCertified: 0,
                            //   nbrToBeCertified: 0,
                            // ),
                            // const SizedBox(height: 10),
                            Padding(
                              padding:
                                  const EdgeInsets.fromLTRB(8.0, 0, 0, 8.0),
                              child: Consumer<MyTheme>(
                                  builder: (context, theme, _) {
                                return IconButton(
                                    color: Colors.white,
                                    icon: Icon(theme.isDark
                                        ? Icons.light_mode
                                        : Icons.dark_mode),
                                    onPressed: () {
                                      theme.switchTheme();
                                    });
                              }),
                            ),
                            const SmithCategory(categoryNumber: 0),
                            const SizedBox(height: 10),
                            GridWidget(
                                nTule: nTule, smithsTiles: smithsTilesOnline),
                            const SizedBox(height: 40),
                            const SmithCategory(categoryNumber: 1),
                            const SizedBox(height: 10),
                            GridWidget(
                                nTule: nTule, smithsTiles: smithsTilesOffline),
                            const SizedBox(height: 40),
                            const SmithCategory(categoryNumber: 2),
                            const SizedBox(height: 10),
                            GridWidget(
                                nTule: nTule,
                                smithsTiles: smithsTilesCertified),
                            const SizedBox(height: 40),
                            const SmithCategory(categoryNumber: 3),
                            const SizedBox(height: 10),
                            GridWidget(
                                nTule: nTule,
                                smithsTiles: smithsTilesRequested),
                          ]);
                    }),
                  ),
                  const SmithFixedColumn(
                    enabled: true,
                  ),
                ],
              );
            },
          ),
        ),
      );
    });
  }
}

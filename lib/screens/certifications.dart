import 'package:accordion/accordion.dart';
import 'package:accordion/controllers.dart';
import 'package:auto_route/annotations.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/smiths_provider.dart';
import 'package:gecko_web/widgets/certs_counter.dart';
import 'package:gecko_web/widgets/certs_list.dart';
import 'package:provider/provider.dart';

@RoutePage()
class CertificationsScreen extends StatelessWidget {
  const CertificationsScreen(
      {super.key,
      required this.address,
      required this.username,
      required this.isSmith});
  final String address;
  final String username;
  final bool isSmith;

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    homeProvider.showCertsView = false;
    final smithP = Provider.of<Smiths>(context, listen: false);
    smithP.showCertsView = false;

    return Scaffold(
        backgroundColor: Theme.of(context).primaryColorDark,
        appBar: AppBar(
            elevation: 0,
            toolbarHeight: 60,
            backgroundColor: Theme.of(context).primaryColorDark,
            leading: IconButton(
                icon: const Icon(Icons.arrow_back, color: orangeC),
                onPressed: () {
                  homeProvider.reload();
                  smithP.reload();
                }),
            title: SizedBox(
              height: 22,
              child: Text(
                'certificationsOf'.tr(args: [username]),
                style: TextStyle(
                    color: Theme.of(context).textTheme.bodyMedium!.color),
              ),
            )),
        body: SafeArea(
          child: Accordion(
              paddingListTop: 10,
              paddingListBottom: 10,
              maxOpenSections: 1,
              headerBackgroundColorOpened: orangeC,
              scaleWhenAnimating: true,
              openAndCloseAnimation: true,
              headerPadding:
                  const EdgeInsets.symmetric(vertical: 7, horizontal: 15),
              sectionOpeningHapticFeedback: SectionHapticFeedback.heavy,
              sectionClosingHapticFeedback: SectionHapticFeedback.light,
              children: [
                AccordionSection(
                  isOpen: true,
                  leftIcon:
                      const Icon(Icons.insights_rounded, color: Colors.black),
                  contentBackgroundColor:
                      Theme.of(context).scaffoldBackgroundColor,
                  contentBorderColor: Theme.of(context).scaffoldBackgroundColor,
                  headerBackgroundColor: yellowC,
                  headerBackgroundColorOpened: orangeC,
                  header: Row(children: [
                    Text('received'.tr(),
                        style:
                            const TextStyle(fontSize: 20, color: Colors.black)),
                    const SizedBox(width: 5),
                    CertsCounter(
                        address: address, isSent: false, isSmith: isSmith)
                  ]),
                  content: CertsList(
                      address: address, isSend: false, isSmith: isSmith),
                  contentHorizontalPadding: 0,
                  contentBorderWidth: 1,
                ),
                AccordionSection(
                  isOpen: false,
                  leftIcon:
                      const Icon(Icons.insights_rounded, color: Colors.black),
                  contentBackgroundColor:
                      Theme.of(context).scaffoldBackgroundColor,
                  contentBorderColor: Theme.of(context).scaffoldBackgroundColor,
                  headerBackgroundColor: yellowC,
                  headerBackgroundColorOpened: orangeC,
                  header: Row(children: [
                    Text(
                      'sent'.tr(),
                      style: const TextStyle(fontSize: 20, color: Colors.black),
                    ),
                    const SizedBox(width: 5),
                    CertsCounter(
                        address: address, isSent: true, isSmith: isSmith)
                  ]),
                  content: CertsList(
                      address: address, isSend: true, isSmith: isSmith),
                  contentHorizontalPadding: 0,
                  contentBorderWidth: 1,
                ),
              ]),
        ));
  }
}

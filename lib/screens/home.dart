// ignore_for_file: avoid_web_libraries_in_flutter, avoid_print, must_be_immutable

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_portal/flutter_portal.dart';
import 'package:gecko_web/global.dart';
import 'package:gecko_web/providers/home.dart';
import 'package:gecko_web/providers/my_theme.dart';
import 'package:gecko_web/router.dart';
import 'package:gecko_web/screens/certifications.dart';
import 'package:gecko_web/widgets/loading.dart';
import 'package:gecko_web/widgets/main_menu.dart';
import 'package:gecko_web/widgets/my_wallets_list.dart';
import 'package:gecko_web/widgets/search_identity.dart';
import 'package:gecko_web/widgets/widgets_home.dart';
import 'package:gecko_web/widgets/search_zone.dart';
import 'package:provider/provider.dart';

@RoutePage()
class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isProfileOpen = false;
  bool isProfileHover = false;

  // Enable/Disable login feature
  final loginEnabled = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    homeContext = context;
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    final theme = Provider.of<MyTheme>(context, listen: false);

    PortalTarget(
      visible: isProfileOpen,
      portalFollower: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          setState(() {
            isProfileOpen = false;
          });
        },
      ),
      child: const Text('data'),
    );

    return FutureBuilder(
        future: homeProvider.asyncStartup(),
        builder: (context, browserInfo) {
          if (browserInfo.connectionState != ConnectionState.done) {
            return const Material(
              child: Column(
                children: [
                  Spacer(flex: 1),
                  Loading(size: 30, stroke: 3),
                  Spacer(flex: 2),
                  // Text('Connexion en cours...')
                ],
              ),
            );
          }
          screenWidth = MediaQuery.of(context).size.width;
          screenHeight = MediaQuery.of(context).size.height;
          homeProvider.isLargeScreen = screenWidth >= screenBreakpoint;
          homeProvider.applyPath();

          return Portal(
            child: Scaffold(
              key: scaffoldKey,
              endDrawer:
                  Consumer<HomeProvider>(builder: (context, homeProvider, _) {
                return Drawer(
                  width: 500,
                  child: homeProvider.showCertsView
                      ? CertificationsScreen(
                          address: homeProvider.currentAddress,
                          username: homeProvider.currentName,
                          isSmith: false)
                      : AutoRouter.declarative(
                          routes: (handler) => [
                                ProfileViewRoute(
                                    address: homeProvider.currentAddress),
                              ]),
                );
              }),
              body: Stack(children: [
                const Positioned(
                  left: 10,
                  bottom: 40,
                  child: MyWalletsList(),
                ),
                SizedBox(
                  width: screenWidth,
                  height: screenHeight,
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Spacer(),
                        Stack(children: [
                          SizedBox(
                            width: screenWidth -
                                (homeProvider.isLargeScreen ? 500 : 0),
                            child: const SingleChildScrollView(
                              child: Column(
                                children: [
                                  Image(
                                      image: AssetImage('assets/header.png'),
                                      height: 150),
                                  SizedBox(height: 120),
                                  SearchZone(),
                                  SizedBox(height: 10),
                                  SearchIdentity(),
                                ],
                              ),
                            ),
                          ),
                          Visibility(
                            visible: loginEnabled,
                            child: Positioned(
                              right: 70,
                              top: 50,
                              child: PortalTarget(
                                portalFollower: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      color: Colors.grey[300],
                                    ),
                                    height: 150,
                                    width: 300,
                                    child: Column(
                                      children: [
                                        const SizedBox(height: 20),
                                        Text(
                                          'Mnemonic:',
                                          style: TextStyle(
                                              color: Colors.grey[800]),
                                        ),
                                        const SizedBox(height: 5),
                                        SizedBox(
                                          width: 200,
                                          child: TextField(
                                              autofocus: true,
                                              style: const TextStyle(
                                                fontSize: 14,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w400,
                                              ),
                                              controller: homeProvider
                                                  .mnemonicController),
                                        ),
                                        const SizedBox(height: 5),
                                        ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              foregroundColor: Colors.white,
                                              elevation: 2,
                                              backgroundColor:
                                                  orangeC, // foreground
                                            ),
                                            onPressed: () =>
                                                log.d('import wallet !'),
                                            child: const Text('Importer'))
                                      ],
                                    )),
                                anchor: const Aligned(
                                  follower: Alignment.centerRight,
                                  target: Alignment.bottomLeft,
                                ),
                                visible: isProfileOpen,
                                child: Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                      color: (isProfileHover || isProfileOpen)
                                          ? orangeC
                                          : Colors.grey[800]!,
                                      width: 3,
                                    ),
                                  ),
                                  child: InkWell(
                                    onHover: (value) {
                                      setState(() {
                                        isProfileHover = value;
                                      });
                                    },
                                    // overlayColor: MaterialStateProperty.all(orangeC),
                                    onTap: () {
                                      isProfileOpen = !isProfileOpen;
                                      setState(() {});
                                    },
                                    child: CircleAvatar(
                                      radius: 20,
                                      backgroundColor: Colors.grey[300],
                                      backgroundImage: const AssetImage(
                                          'assets/avatars/0-1.png'),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ]),
                        const Spacer(),
                        if (homeProvider.isLargeScreen)
                          Consumer<HomeProvider>(builder: (context, homeP, _) {
                            return Container(
                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColorDark,
                                boxShadow: [
                                  BoxShadow(
                                    color: theme.isDark
                                        ? Theme.of(context).primaryColorDark
                                        : Colors.grey.withOpacity(0.5),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: const Offset(0, 3),
                                  ),
                                ],
                              ),
                              width: 500,
                              child: homeProvider.currentAddress == ''
                                  ? const InfosView()
                                  : homeProvider.showCertsView
                                      ? CertificationsScreen(
                                          address: homeProvider.currentAddress,
                                          username: homeProvider.currentName,
                                          isSmith: false)
                                      : AutoRouter.declarative(
                                          routes: (handler) => [
                                                ProfileViewRoute(
                                                    address: homeProvider
                                                        .currentAddress),
                                              ]),
                            );
                          }),
                      ]),
                ),
                const Positioned(
                  left: 10,
                  top: 150,
                  child: MainMenu(),
                ),
                Positioned(
                  left: 10,
                  top: screenHeight - 20,
                  child: Text(
                    appVersion,
                    style: const TextStyle(fontSize: 9),
                  ),
                ),
                Positioned(
                  left: 70,
                  top: 50,
                  child: Consumer<MyTheme>(builder: (context, theme, _) {
                    return IconButton(
                        icon: Icon(
                            theme.isDark ? Icons.light_mode : Icons.dark_mode),
                        onPressed: () {
                          theme.switchTheme();
                        });
                  }),
                ),
              ]),
            ),
          );
        });
  }
}

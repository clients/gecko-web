import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/providers/polkadot.dart';
import 'package:gecko_web/widgets/widget_pay.dart';
import 'package:gecko_web/widgets/widgets_activity.dart';
import 'package:gecko_web/widgets/header_profile.dart';
import 'package:provider/provider.dart';

@RoutePage(name: 'ProfileViewRoute')
class ProfileView extends StatelessWidget {
  const ProfileView({super.key, @PathParam('address') required this.address});

  final String address;

  @override
  Widget build(BuildContext context) {
    final polka = Provider.of<PolkadotProvider>(context, listen: false);

    return Column(children: <Widget>[
      const HeaderProfileView(),
      const HistoryQuery(),
      if ((polka.haveExtension ?? false) && polka.listMyWallets.isNotEmpty)
        const PayPopup()
    ]);
  }
}

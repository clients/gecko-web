class WalletData {
  String address;
  String name;
  String? idtyName;
  WalletType type;
  IdentityStatus status;

  WalletData({required this.address, required this.name, this.idtyName, this.type = WalletType.sr25519, this.status = IdentityStatus.none});
}

enum WalletType { sr25519, ed25519 }

enum IdentityStatus { none, unconfirmed, unvalidated, member, notMember, revoked, unknown }

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:gecko_web/screens/certifications.dart';
import 'package:gecko_web/screens/home.dart';
import 'package:gecko_web/screens/profile_view.dart';
import 'package:gecko_web/screens/smiths.dart';

part 'router.gr.dart';

@AutoRouterConfig()
class AppRouter extends RootStackRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(path: '/', page: MyHomeRoute.page, children: [
          AutoRoute(path: 'profile/:address', page: ProfileViewRoute.page, children: [
            AutoRoute(path: 'certifications', page: CertificationsRoute.page),
          ])
        ]),
        AutoRoute(path: '/smiths', page: SmithsRoute.page),
        RedirectRoute(path: '/profile', redirectTo: '/'),
      ];
}

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'router.dart';

/// generated route for
/// [CertificationsScreen]
class CertificationsRoute extends PageRouteInfo<CertificationsRouteArgs> {
  CertificationsRoute({
    Key? key,
    required String address,
    required String username,
    required bool isSmith,
    List<PageRouteInfo>? children,
  }) : super(
          CertificationsRoute.name,
          args: CertificationsRouteArgs(
            key: key,
            address: address,
            username: username,
            isSmith: isSmith,
          ),
          initialChildren: children,
        );

  static const String name = 'CertificationsRoute';

  static PageInfo page = PageInfo(
    name,
    builder: (data) {
      final args = data.argsAs<CertificationsRouteArgs>();
      return CertificationsScreen(
        key: args.key,
        address: args.address,
        username: args.username,
        isSmith: args.isSmith,
      );
    },
  );
}

class CertificationsRouteArgs {
  const CertificationsRouteArgs({
    this.key,
    required this.address,
    required this.username,
    required this.isSmith,
  });

  final Key? key;

  final String address;

  final String username;

  final bool isSmith;

  @override
  String toString() {
    return 'CertificationsRouteArgs{key: $key, address: $address, username: $username, isSmith: $isSmith}';
  }
}

/// generated route for
/// [MyHomePage]
class MyHomeRoute extends PageRouteInfo<void> {
  const MyHomeRoute({List<PageRouteInfo>? children})
      : super(
          MyHomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'MyHomeRoute';

  static PageInfo page = PageInfo(
    name,
    builder: (data) {
      return const MyHomePage();
    },
  );
}

/// generated route for
/// [ProfileView]
class ProfileViewRoute extends PageRouteInfo<ProfileViewRouteArgs> {
  ProfileViewRoute({
    Key? key,
    required String address,
    List<PageRouteInfo>? children,
  }) : super(
          ProfileViewRoute.name,
          args: ProfileViewRouteArgs(
            key: key,
            address: address,
          ),
          rawPathParams: {'address': address},
          initialChildren: children,
        );

  static const String name = 'ProfileViewRoute';

  static PageInfo page = PageInfo(
    name,
    builder: (data) {
      final pathParams = data.inheritedPathParams;
      final args = data.argsAs<ProfileViewRouteArgs>(
          orElse: () =>
              ProfileViewRouteArgs(address: pathParams.getString('address')));
      return ProfileView(
        key: args.key,
        address: args.address,
      );
    },
  );
}

class ProfileViewRouteArgs {
  const ProfileViewRouteArgs({
    this.key,
    required this.address,
  });

  final Key? key;

  final String address;

  @override
  String toString() {
    return 'ProfileViewRouteArgs{key: $key, address: $address}';
  }
}

/// generated route for
/// [SmithsPage]
class SmithsRoute extends PageRouteInfo<void> {
  const SmithsRoute({List<PageRouteInfo>? children})
      : super(
          SmithsRoute.name,
          initialChildren: children,
        );

  static const String name = 'SmithsRoute';

  static PageInfo page = PageInfo(
    name,
    builder: (data) {
      return const SmithsPage();
    },
  );
}

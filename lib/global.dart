import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:logger/logger.dart';

const String currencyName = 'ĞD';

String? currentDuniterEndpoint;
List? listDuniterEndpoints;

List? listIndexerEndpoints;
String? currentIndexerEndpoint;

const screenBreakpoint = 1130;
late String appVersion;

// Colors
const Color orangeC = Color(0xffd07316);
const Color yellowC = Color(0xffFFD68E);
const Color floattingYellow = Color(0xffEFEFBF);
const Color backgroundColor = Color(0xFFF5F5F5);

// Style
TextStyle mainStyle = const TextStyle(
  fontSize: 20,
  decoration: TextDecoration.none,
  color: Colors.black,
);
TextStyle mainStyle16 = const TextStyle(
  fontSize: 16,
  decoration: TextDecoration.none,
  color: Colors.black,
);

// Screen size
late double screenWidth;
late double screenHeight;

late BuildContext homeContext;

// Logger
var log = Logger();

// Hive box
late Box configBox;

// Indexer
late DateTime startBlockchainTime;

// keys
final scaffoldKey = GlobalKey<ScaffoldState>();

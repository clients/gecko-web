# Ğecko web

Web implementation of Ğecko ĞDev wallet.
This is readonly (explore) until Ğ1-connect web extension is ready.

## Build for web

```sh
# build for web in html mode
flutter build web --web-renderer canvaskit
# deploy files
rsync -cvrl build/web/* user@host:/path/  
```
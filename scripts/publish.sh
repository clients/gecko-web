#!/bin/bash

set -e

if [[ $1 == "html" ]]; then
	rm -rf build/web && flutter build web --web-renderer html
	ssh -p 10422 poka@pokahome.p2p.legal 'rm -rf /home/poka/gecko-html && mkdir /home/poka/gecko-html'
	rs build/web poka@pokahome.p2p.legal:/home/poka/gecko-html/ 10422
else
        rm -rf build/web && flutter build web --web-renderer canvaskit #auto
        ssh -p 10422 poka@pokahome.p2p.legal 'rm -rf /home/poka/gecko_web && mkdir -p /home/poka/gecko_web'
        rs build/web poka@pokahome.p2p.legal:/home/poka/gecko_web/ 10422
fi
